/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : divideCoord.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */



import Zdevs.*;
import Zdevs.Zcontainer.*;

public class divideCoord extends Coord{

int num_procs, num_results;

public divideCoord(String   name){
super(name);
num_procs = 0;
num_results = 0;
}

public divideCoord(){
super("divideCoord");

//CAUTION: start with port "setup" to test

addTestPortValue("setup",new entity(""));
addTestPortValue("in",new entity("val"));
addTestPortValue("x",new entity("val"));
message m = new message();
m.add(make_content("in", new entity("val1")));
m.add(make_content("in",new entity("val2")));
addTest("multiple inputs", m);

initialize();
}

public void initialize(){
     phase = "passive";
     sigma = INFINITY;
     job = null;
     super.initialize();;
 }

public void showState(){
    super.showState();
    addString("num_procs: " + num_procs);
    addString("num_results: " + num_results);
}

 public void myRepaint(){
    super.myRepaint();
 if ( myAtomicDrawPanel != null){
 myAtomicDrawPanel.graph.SetPhaseElapsed(num_results-5,e/100,phase);
 myAtomicDrawPanel.repaint();
 }
 }

protected void add_procs(proc   p){
num_procs++;
num_results++;
}

public void  deltext(int e,message   x)
{

Continue(e);

if (phase_is("passive"))
{
 for (int i=0; i< x.get_length();i++)
 if (message_on_port(x,"setup",i))
     add_procs(new proc("p",1000));
}

if (phase_is("passive"))
{
 for (int i=0; i< x.get_length();i++)
 if (message_on_port(x,"in",i))
   {
   job = x.get_val_on_port("in",i);
       num_results = num_procs;
       hold_in("send_y",100);
   }

}
// job pairs returned on port x


else if (phase_is("busy")){
 for (int i=0; i< x.get_length();i++)
 if (message_on_port(x,"x",i))
    {
      num_results--;
      if (num_results == 0)
           hold_in("send_out",100);
    }
}

 show_state();
}

public void  deltint( )
{
if (phase_is("send_y"))
     passivate_in("busy");
else passivate();
show_state();
}

public message    out( )
{
message   m = new message();
if (phase_is("send_out"))
  m.add( make_content("out",job));
else if (phase_is("send_y"))
    m.add(make_content("y",job));
return m;
}

public void show_state()
{
System.out.println(   "state of  " +  name +  ": " );
System.out.println(  "phase, sigma,job,results : "
       + phase +  " " +  sigma + " " + job +
       /* job.get_name()  + */ //null.get_name() causes problems
               " " + num_results);
  System.out.println( );
}


}


