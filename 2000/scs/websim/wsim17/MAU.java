/***************************************************************
	ECE 575
	Project:  Distributed Object Computing
	Daryl Hild

File:  MAU.java

           +--------MAU------------+
     in    |  in        out        |  media_out
     ----->|---->MAUxmtr---------->|---------->
           |     ^                 |
           |     |media_in         |
           |     |                 |
  media_in |     | in        out   |       out
  -------->|-----+--->MAUrcvr----->|---------->
           |                       |
           +-----------------------+

Assumptions:
     1. Only input and output ports declared below are legitimate.


DEVS DIGRAPH-MODEL:  MAU
     composition tree:  root:  MAU
                      leaves:  XMTR, RCVR
     external input coupling:  MAU.in       -> XMTR.in
                               MAU.media_in -> XMTR.media_in
                               MAU.media_in -> RCVR.in
     external output coupling: XMTR.out -> MAU.media_out
                               RCVR.out -> MAU.out
     internal input coupling:  none
     influencee digraph:       none
     priority list:            RCVR, XMTR

***************************************************************/


import java.lang.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;


public class MAU extends digraph {


	public MAU() {
		super("MAU");
	    int PropBytes=16, ZeroTime=100, RcvSvcTime=5;
		make("MAU", PropBytes, ZeroTime, RcvSvcTime);
	}

	public MAU(String name, int PropBytes, int ZeroTime, int RcvSvcTime) {
	    super(name);
	    make(name, PropBytes, ZeroTime, RcvSvcTime);
	}

	private void make(String name, int PropBytes, int ZeroTime, int RcvSvcTime) {
		atomic XMTR = new MAUxmtr(name, PropBytes, ZeroTime);
		atomic RCVR = new MAUrcvr(name, RcvSvcTime, ZeroTime);
		add(XMTR);
		add(RCVR);
	// initialize
		addTestPortValue("in", new pair(
		  new entity("dst1"),
		  new pair(new intEnt(1500),new entity("payload_1"))));
		addTestPortValue("in", new pair(
		  new entity("dst2"),
		  new pair(new intEnt(500),new entity("payload_2"))));
		initialize();
	//	show_state();
	// external input coupling
		Add_coupling(this,"in",XMTR,"in");
		Add_coupling(this,"media_in",XMTR,"media_in");
		Add_coupling(this,"media_in",RCVR,"in");
	// external output coupling
		Add_coupling(XMTR,"out",this,"media_out");
		Add_coupling(RCVR,"out",this,"out");
	// internal coupling
		// none
	//	show_coupling();
	}
}
