/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  delay.java
 *  Version    :  1.0
 *  Date       :  12-9-96
 */

package Zdevs;

public class delay{
 int length;

 public boolean isDone(){
  if(System.currentTimeMillis() >= start + length)
    return true;
  return false;
 }

 boolean justOnNoted;

 public boolean isJustOn(){
  if(justOnNoted) return false;
    justOnNoted = true;
	return true;
  }

 public delay(int length_){
  start = System.currentTimeMillis();
  length = length_;
 }

 long start;
}

