# LaTeX2HTML 97.1 (release) (July 13th, 1997)
# Associate images original text with physical files.


$key = q/{figure}centerhtmlimagescale=1.5includegraphicsgaps-classic.pscenter{figure}FSF=1;AAT;/;
$cached_env_img{$key} = q|<IMG WIDTH="256" HEIGHT="119"
 SRC="img1.gif"
 ALT="\begin{figure}
 \begin{center}
 
\includegraphics {gaps-classic.ps}
 \end{center}\end{figure}">|; 

$key = q/{figure}centerhtmlimagescale=2includegraphics[scale=0.6]gapsed.pscenter{figure}FSF=1;AAT;/;
$cached_env_img{$key} = q|<IMG WIDTH="331" HEIGHT="254"
 SRC="img5.gif"
 ALT="\begin{figure}
 \begin{center}
 
\includegraphics [scale=0.6]{gapsed.ps}
 \end{center}\end{figure}">|; 

$key = q/{figure}centerhtmlimagescale=2.5includegraphics[width=linewidth]gaps-script.pscenter{figure}FSF=1;AAT;/;
$cached_env_img{$key} = q|<IMG WIDTH="537" HEIGHT="390"
 SRC="img7.gif"
 ALT="\begin{figure}
 \begin{center}
 
\includegraphics [width=\linewidth]{gaps-script.ps}

 \end{center}\end{figure}">|; 

$key = q/{figure}centerhtmlimagescale=2includegraphicssimvisu.pscenter{figure}FSF=1;AAT;/;
$cached_env_img{$key} = q|<IMG WIDTH="369" HEIGHT="162"
 SRC="img8.gif"
 ALT="\begin{figure}
 \begin{center}
 
\includegraphics {simvisu.ps}
 \end{center}\end{figure}">|; 

$key = q/{figure}centerhtmlimagescale=2includegraphics[width=linewidth]gaps-docu.pscenter{figure}FSF=1;AAT;/;
$cached_env_img{$key} = q|<IMG WIDTH="422" HEIGHT="169"
 SRC="img4.gif"
 ALT="\begin{figure}
 \begin{center}
 
\includegraphics [width=\linewidth]{gaps-docu.ps}
 \end{center}\end{figure}">|; 

$key = q/{figure}centerhtmlimagescale=2.5includegraphics[width=0.9linewidth]gapsarch.pscenter{figure}FSF=1;AAT;/;
$cached_env_img{$key} = q|<IMG WIDTH="482" HEIGHT="261"
 SRC="img3.gif"
 ALT="\begin{figure}
 \begin{center}
 
\includegraphics [width=0.9\linewidth]{gapsarch.ps}
 \end{center}\end{figure}">|; 

$key = q/{figure}centerhtmlimagescale=2includegraphics[width=0.49linewidth]gapsed-coop-lipsincludegraphics[width=0.49linewidth]gapsed-coop-sun.pscenter{figure}FSF=1;AAT;/;
$cached_env_img{$key} = q|<IMG WIDTH="211" HEIGHT="310"
 SRC="img6.gif"
 ALT="\begin{figure}
 \begin{center}
 
\includegraphics [width=0.49\linewidth]{gapsed-...
 ...udegraphics [width=0.49\linewidth]{gapsed-coop-sun.ps}
 \end{center}\end{figure}">|; 

$key = q/{figure}centerhtmlimagescale=2includegraphics[scale=0.6]petri-net.pscenter{figure}FSF=1;AAT;/;
$cached_env_img{$key} = q|<IMG WIDTH="465" HEIGHT="258"
 SRC="img2.gif"
 ALT="\begin{figure}
 \begin{center}
 
\includegraphics [scale=0.6]{petri-net.ps}
 \end{center}\end{figure}">|; 

1;

