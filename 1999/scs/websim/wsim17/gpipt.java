/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : gpipt.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


import java.lang.*;
import java.awt.*;
import java.io.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;

public class gpipt extends digraph{

 public gpipt(){
  super("gpipt");
  atomic m1 = new genr("g",1500);
  digraph m2 = new pipeLine("pipe",1600,2);
  atomic m5 = new transd("transd",10000);
  add(m1);
  add(m2);
  add(m5);
  addTestPortValue("start",new entity("val"));
  initialize();
  show_state();
  Add_coupling(m1,"out",m2,"in");
  Add_coupling(m1,"out",m5,"ariv");
  Add_coupling(m2,"out",m5,"solved");
  Add_coupling(m5,"out",m1,"stop");
  Add_coupling(this,"start",m1,"start");
  Add_coupling(m2,"out",this,"out");
  show_coupling();
  //inject("start", new entity("val"));
 }
}