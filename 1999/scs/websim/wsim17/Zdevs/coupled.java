/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : coupled.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;
import Zdevs.Zcontainer.*;

public class coupled extends coupled1{
 public void remove(devs d){
 components.remove(d);
 // System.out.println("============");components.print();
 }

 public void reInitialize(devs d){
  comp_map.print();
  /*
  comp_map = new function();
  int i = 0;
  for(entity  p1=components.get_head(); p1!=null; p1=p1.get_right()) {
    entity pp = p1.get_ent();
    intEnt  temp = new intEnt(i);
    comp_map.ADD(pp, temp);
    i++;
  }
  */
  intEnt  temp = new intEnt(components.get_length()-1);
  comp_map.ADD(d, temp);
  comp_map.print();
 }

 protected function comp_map;
 protected set components;

 public coupled(String nm){
  super(nm);
  comp_map = new function();
  components = new set();
  set_parent(null);
 }

 public void add(devs b){
  //components.add(b);//zeigler changed from ADD(b);
  set s = reverse(components);//zeigler changed for Var Struct);
  s. add(b);
  components = reverse(s);
  b.set_parent(this);
 }


 public set reverse(set comps){
  // To reverse the container
  set r = new set();
  for(entity p = comps.get_head();p != null;p = p.get_right()){
     entity pp = p.get_ent();
     r.add(pp);//need add since multiple
     //r.ADD(p);
   }
   return r;
 }

 public set get_components(){
  return components;
 }

 public set get_atomic_components(){
  set r = new set();
  for(entity p = components.get_head();p != null;p = p.get_right()){
     entity ent = p.get_ent();
     devs d = (devs)ent;
     set s = d.get_atomic_components();
     r.union_self(s);
  }
  return reverse(r);
}

 public void initialize(){
  super.initialize();
  input = null; //needed for deltext
  int i = 0;
  for(entity  p1=components.get_head(); p1!=null; p1=p1.get_right()) {
     entity pp = p1.get_ent();
     ((devs)pp).initialize();
     intEnt  temp = new intEnt(i);
     comp_map.ADD(pp, temp);
     i++;
  }
  //comp_map->print();
 }

 public void  simulate(int num_iter){
  int save = 0;
  show_tL();
  int i=1;
  tN = next_tN();
  //while( (tN < INFINITY) && (i<=num_iter) ){
  while( (i<=num_iter) ) {
  show_tN();
  System.out.println("ITERATION " + i );
  if(save + 100 <= i ){
    System.out.println(i);
    show_tN();
    save = i;
  }
  compute_input_output(tN);
  //System.out.println("after compio");
  tellall_wrap_deltfunc(tN,input);
  tL = tN;
  tN = next_tN();
  show_state();
  show_tL();
  i++;
  }
 }

 public int next_tN(){
  int t;
  int minval = INFINITY;
  for(entity p = components.get_head();p!= null;p=p.get_right()){
     entity pp = p.get_ent();
     if (minval > (t = ((devs) (pp)).next_tN())){
     minval = t;
  }
 }

 //System.out.println("COUPLED : ["+ get_name() + "] next tN = " + minval);
 tN = minval;
 return  minval;
 }

 public void tellall_compute_input_output(int t){
  for(entity p = components.get_head();p != null;p = p.get_right()){
     entity pp = p.get_ent();
    ((devs) pp).compute_input_output(t);
  }
 }


 public void show_state(){
  for(entity p = components.get_head();p != null;p = p.get_right()){
     entity pp = p.get_ent();
    ((devs)pp).show_state();
  }
 }

 public void wrap_deltfunc(int t, message msg) {
  /*
  #ifdef DEBUGG
    cout << "COUPLED: [" << get_name() << "] WRAP_DELTFUNC() tN = "
   <<tN << endl;
   cout << endl;
   #endif
 */
 if(msg.empty() && t != tN)
   return;
 deltfunc(t,msg);
 tL = t;
 tN = next_tN();
 }

 public void tellall_wrap_deltfunc(int tN, message m) {}

 public void deltfunc(int t, message m){}
}
