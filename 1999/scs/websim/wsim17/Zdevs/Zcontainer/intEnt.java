/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  intEnt.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs.Zcontainer;
import java.lang.*;

public class intEnt extends entity {
 int v;

 public intEnt(int t){
  v = t;
 }

 public boolean greater_than(entity ent){
  return (this.v > ((intEnt)  ent).getv());
 }

 public void setv(int t){
  v = t;
 }

 public int getv(){
  return v;
 }

 public void print(){
  System.out.print( v);
 }

 public boolean equal(entity ent){
  return (this.v == ((intEnt)  ent).getv());
 }

 public entity  copy(){
  intEnt  ip = new intEnt(getv());
   return (entity)ip;
 }

 public String get_name(){
  return Integer.toString(v);
 }
}
