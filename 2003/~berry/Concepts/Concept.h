// Concept.h
// D�finition de la classe Concept

#include "Methodes.cpp"
#include <stack>

using namespace std;

#ifndef CONCEPT_H
#define CONCEPT_H

typedef Matrice<int> MatInt;
typedef Matrice<VecInt> MatVecInt;

class Concept
{
 protected:
  VecInt pte;	   // Propri�t�s du concept
  VecInt obj;	   // Objets du concept
  MatInt R;	      // Relation R r�duite
  MatVecInt N;	   // Liste des N+ dans la relation
  MatVecInt M;    // Liste des maxmods
  MatVecInt ND;   // Liste des maxmods non dominants
  MatVecInt DOM;  // Tableau des dominations
  VecInt Marked;  // Liste des proprietes deja traitees

 public:
  Concept();
  Concept(VecInt _pte,VecInt _obj);
  Concept(VecInt _pte,VecInt _obj,VecInt _Marked);
  Concept(const Concept& _concept);
  int nbPte()const;
  int nbObj()const;
  void CONCEPTS(const MatInt& _R,MatInt& T,MatInt& D,int nbp,int nbo,MatVecInt& CONCEPTS,MatVecInt& REGLES);
  	void ajouterConcept(MatVecInt& CONCEPTS)const;
  	void initialisation(const MatInt& _R,int nbo,int nbp);
	  MatInt construireR(const MatInt& _R,int nbo,int nbp)const;
	  MatVecInt construireN()const;
	  MatVecInt construireM()const;
	  void majMarked();
   void calculND_DOM(int nbp,MatInt& T,MatInt& D);
   void REGLES_ASSOCIATION(MatVecInt& REGLES);
  	void generationConcepts(MatInt& T,MatInt& D,int nbp,int nbo,MatVecInt& CONCEPTS,MatVecInt& REGLES);
	  void UPDATE(bool V,MatInt& T,MatInt& D,int elem,int nbp,int nbo);
  ~Concept();
};

Concept::Concept(){
}

Concept::Concept(VecInt _pte,VecInt _obj){
  pte=_pte;
  obj=_obj;
}

Concept::Concept(VecInt _pte,VecInt _obj,VecInt _Marked){
  pte=_pte;
  obj=_obj;
  Marked=_Marked;
}

Concept::Concept(const Concept& _concept){
  pte=_concept.pte;
  obj=_concept.obj;
  R=_concept.R;
  N=_concept.N;
  M=_concept.M;
  ND=_concept.ND;
  Marked=_concept.Marked;
}

int Concept::nbPte()const{
  return pte.size();
}

int Concept::nbObj()const{
  return obj.size();
}

// Algorithme CONCEPTS
void Concept::CONCEPTS(const MatInt& _R,MatInt& T,MatInt& D,int nbp,int nbo,MatVecInt& CONCEPTS,MatVecInt& REGLES){
  ajouterConcept(CONCEPTS);
  initialisation(_R,nbo,nbp);
  calculND_DOM(nbp,T,D);
  REGLES_ASSOCIATION(REGLES);
  generationConcepts(T,D,nbp,nbo,CONCEPTS,REGLES);
}

// M�thode ins�rant le nouveau concept dans la matrice CONCEPTS
void Concept::ajouterConcept(MatVecInt& CONCEPTS)const{
  if (pte.size()!=0){
	MatVecInt M(1,2);
	M.setElement(0,0,pte);
	M.setElement(0,1,obj);
	CONCEPTS.ajoutLigne(M);
  }
}

// M�thode d'initialisation d'un concept ie construisant R, N, M et mettant � jour Marked
void Concept::initialisation(const MatInt& _R,int nbo,int nbp){
  R=construireR(_R,nbo,nbp);
  N=construireN();
  M=construireM();
  majMarked();
}

// M�thode construisant la relation r�duite R
MatInt Concept::construireR(const MatInt& _R,int nbo,int nbp)const{
  MatInt R_temp(obj.size()+1,nbp-pte.size());
  int k1=-1;
  for(int i=0;i<_R.getNbCol();i++)
    if (appartient(pte,_R.getElement(0,i))==0){
      k1++;
      R_temp.setElement(0,k1,_R.getElement(0,i));
    }
  int k2;
  k1=-1;
  for(int i=0;i<_R.getNbCol();i++)
    if (appartient(pte,_R.getElement(0,i))==0){
      k1++;
      k2=0;
      for (int j=1;j<_R.getNbLigne();j++)
        if (appartient(obj,_R.getElement(j,0))==1){
	      k2++;
	      R_temp.setElement(k2,k1,_R.getElement(j,i));
	    }
    }
  return R_temp;
}

// M�thode construisant la liste des N+ dans la relation
MatVecInt Concept::construireN()const{
  int nbl=R.getNbLigne();
  int nbc=R.getNbCol();
  MatVecInt _N(nbl-1,2);
  for (int i=0;i<nbl-1;i++){
    VecInt V2;
    V2.push_back(R.getElement(i+1,0));
    _N.setElement(i,0,V2);
    VecInt V1;
    for (int j=1;j<nbc;j++)
      if (R.getElement(i+1,j)==1) V1.push_back(R.getElement(0,j));
    if (V1.size()==0) V1.push_back(0);
    _N.setElement(i,1,V1);
  }
  return _N;
}

// M�thode construisant la liste des maxmods
MatVecInt Concept::construireM()const{
  MatVecInt _M(1,1);
  VecInt Vinit1;
  for (int i=1;i<R.getNbCol();i++) Vinit1.push_back(R.getElement(0,i));
  _M.setElement(0,0,Vinit1);
  MatVecInt Mtemp(1,2);
  for(int k=0;k<N.getNbLigne();k++)
    if (*N.getElement(k,1).begin()!=0){
       int taille=2*_M.getNbCol();
       if (taille>(int)Vinit1.size()) taille=Vinit1.size();
       Mtemp.setTaille(1,taille);
       int l=0;
       for(int i=0;i<_M.getNbCol();i++){
         VecInt V1,V2;
         for (int j=0;j<(int)_M.getElement(0,i).size();j++){
           if (appartient(N.getElement(k,1),_M.getElement(0,i)[j])==1) V1.push_back(_M.getElement(0,i)[j]);
           else V2.push_back(_M.getElement(0,i)[j]);
         }
         if (V1.size()!=0){
           Mtemp.setElement(0,l,V1);
           l++;
	     }
         if (V2.size()!=0){
           Mtemp.setElement(0,l,V2);
           l++;
	     }
       }
       _M.setTaille(1,l);
       for (int j=0;j<l;j++) _M.setElement(0,j,Mtemp.getElement(0,j));
    }
  return _M;
}

// M�thode de mise � jour de la liste des propri�t�s d�j� marqu�es Marked
void Concept::majMarked(){
  for (int i=0;i<M.getNbCol();i++)
    if (M.getElement(0,i).size()>1)
      for (int j=0;j<(int)M.getElement(0,i).size();j++)
        if (appartient(Marked,M.getElement(0,i)[j])==1){
          Marked=sommeVecInt(Marked,M.getElement(0,i));
          break;
        }
}

// M�thode construisant la liste des maxmods non dominants et le tableau des dominations
void Concept::calculND_DOM(int nbp,MatInt& T,MatInt& D){
  int taille=M.getNbCol();
  int l=-1;
  int c=-1;
  int t=0;
  MatVecInt _ND(1,taille);
  MatVecInt _DOM(nbp-pte.size()-1,2);
  for (int i=0;i<taille;i++){
    t=M.getElement(0,i)[0];
    int k=0;
    while (D.getElement(0,k)!=t) k++;
    if (D.getElement(1,k)==(int)M.getElement(0,i).size()){
      l++;
      _ND.setElement(0,l,M.getElement(0,i));
      if (M.getElement(0,i).size()>1){
        for (int j=0;j<(int)M.getElement(0,i).size();j++){
          c++;
          VecInt V1;
          V1.push_back(M.getElement(0,i)[j]);
          _DOM.setElement(c,0,V1);
        }
      }
    }
    else{
      for (int j=0;j<(int)M.getElement(0,i).size();j++){
        c++;
        VecInt V1;
        V1.push_back(M.getElement(0,i)[j]);
        _DOM.setElement(c,0,V1);
      }
    }
  }
  ND.setTaille(1,l+1);
  for (int i=0;i<l+1;i++) ND.setElement(0,i,_ND.getElement(0,i));
  DOM.setTaille(c+1,2);
  for (int i=0;i<c+1;i++) DOM.setElement(i,0,_DOM.getElement(i,0));
  for (int i=0;i<DOM.getNbLigne();i++){
    VecInt V1;
    int j=*DOM.getElement(i,0).begin();
    for (int k=1;k<nbp;k++)
      if ((k!=j) && (T.getElement(k,j)==0) && (appartient(pte,k)==0)) V1.push_back(k);
    DOM.setElement(i,1,V1);
  }
}

// M�thode g�n�rant les r�gles d'association
void Concept::REGLES_ASSOCIATION(MatVecInt& REGLES){
  int n1=DOM.getNbLigne();
  stack<int> pile;
  int test;
  for (int i=0;i<n1;i++){
    test=0;
    for (int j=0;j<REGLES.getNbLigne();j++){
      VecInt V(sommeVecInt(DOM.getElement(i,0),pte));
      if ((inclus(REGLES.getElement(j,0),V)==1) && (inclus(REGLES.getElement(j,1),V)==0)){
        test=1;
	    break;
      }
    }
    if (test==0){
	  MatVecInt ligne(1,2);
  	  VecInt V1(sommeVecInt(DOM.getElement(i,0),pte));
	  ligne.setElement(0,0,V1);
	  ligne.setElement(0,1,DOM.getElement(i,1));
	  REGLES.ajoutLigne(ligne);
	  for (int j=0;j<REGLES.getNbLigne()-1;j++){
        if ((inclus(REGLES.getElement(REGLES.getNbLigne()-1,0),REGLES.getElement(j,0))==1) && (inclus(REGLES.getElement(REGLES.getNbLigne()-1,1),REGLES.getElement(j,0))==0))
          pile.push(j);
	  }
	  while (!pile.empty()){
        REGLES.supprimLigne(pile.top());
        pile.pop();
  	  }
    }
  }
}

// M�thode g�n�rant les nouveaux concepts
void Concept::generationConcepts(MatInt& T,MatInt& D,int nbp,int nbo,MatVecInt& CONCEPTS,MatVecInt& REGLES){
  for (int i=0;i<ND.getNbCol();i++){
    int k=0;
    for (int j=0;j<(int)ND.getElement(0,i).size();j++)
      if (appartient(Marked,ND.getElement(0,i)[j])==1) k=1;
    if (k==0){
      VecInt V1(ND.getElement(0,i));
      VecInt V2(pte);
      VecInt Vpte;
      Vpte=sommeVecInt(V1,V2);
      int j=1;
      while (*ND.getElement(0,i).begin()!=R.getElement(0,j)) j++;
      VecInt Vobj;
      for (int k=1;k<R.getNbLigne();k++)
        if (R.getElement(k,j)==1) Vobj.push_back(R.getElement(k,0));
        if (Vobj.size()!=0){
          Concept c1(Vpte,Vobj,Marked);
          this->UPDATE(0,T,D,i,nbp,nbo);		//pre
          c1.CONCEPTS(R,T,D,nbp,nbo,CONCEPTS,REGLES);
          this->UPDATE(1,T,D,i,nbp,nbo);		//post
          Marked=sommeVecInt(Marked,ND.getElement(0,i));
        }
    }
  }
}

// M�thode de mise � jour des tables T et D
void Concept::UPDATE(bool V,MatInt& T,MatInt& D,int elem,int nbp,int nbo){
  int x=*ND.getElement(0,elem).begin();
  for (int y=1;y<nbp;y++)
	if ((appartient(pte,y)==0) && (appartient(ND.getElement(0,elem),y)==0))
      if (T.getElement(x,y)==0){
        if (V==0) D.setElement(1,y-1,D.getElement(1,y-1)-ND.getElement(0,elem).size());
  	    else D.setElement(1,y-1,D.getElement(1,y-1)+ND.getElement(0,elem).size());
  	  }
  int l=1;
  while (R.getElement(0,l)!=x && l<=x) l++;
  for (int j=1;j<nbo;j++){
  	int k=1;
    while (R.getElement(k,0)!=j && k<=j) k++;
    if ((R.getElement(k,l)==0) && (appartient(obj,j)==1)){
	  VecInt Z;
  	  for (int i=0;i<N.getNbLigne();i++)
  	    if (*N.getElement(i,0).begin()==j){
	      Z=N.getElement(i,1);
  	      break;
  	    }
  	  for (int i=0;i<(int)Z.size();i++)
  	    for (int k=1;k<nbp;k++)
  	      if ((appartient(pte,k)==0) && (appartient(ND.getElement(0,elem),k)==0) && (appartient(Z,k)==0))
  	        if (V==0){
		  	  T.setElement(k,Z[i],T.getElement(k,Z[i])-1);
		  	  if (T.getElement(k,Z[i])==0)
		  	    D.setElement(1,Z[i]-1,D.getElement(1,Z[i]-1)+1);
	  	    }
	  	    else{
		  	  T.setElement(k,Z[i],T.getElement(k,Z[i])+1);
		  	  if (T.getElement(k,Z[i])==1)
		  	    D.setElement(1,Z[i]-1,D.getElement(1,Z[i]-1)-1);
	  	    }
     }
  }
}

Concept::~Concept(){
}

#endif
