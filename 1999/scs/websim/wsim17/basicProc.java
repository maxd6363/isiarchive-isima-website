/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : basicProc.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */

import java.lang.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;

public class basicProc extends atomic{
    protected entity job;
    protected int processing_time,state;


public basicProc(){
super("basicProc");
addTestPortValue("in",new entity("job"));
addTestPortValue("none",new entity("job"));
addTestPortValue("setProcTime",new intEnt(500));
addTestPortValue("setProcTime",new intEnt(1000));
addTestPortValue("setProcTime",new intEnt(2000));

initialize();
}

public void initialize(){
     phase = "passive";
     sigma = INFINITY;
     job = new entity("job");
     processing_time = 1000;
     super.initialize();

 }

public void showState(){

     super.showState();
     addString("job: "+job.get_name());
     addString("ProcTIme: " + processing_time);

}

public void  deltext(int e,message   x)
{

    Continue(e);

 for (int i=0; i< x.get_length();i++)
      if (message_on_port(x,"setProcTime",i))
      {
     entity en = x.get_val_on_port("setProcTime",i);
      intEnt in = (intEnt)en;
      processing_time = in.getv();
      }
if (phase_is("passive"))
{
 for (int i=0; i< x.get_length();i++)
  if (message_on_port(x,"in",i))
      {
      job = x.get_val_on_port("in",i);
      hold_in("busy",processing_time);
      }
//show_state();
}
}

public void  deltint( )
{
passivate();
job = new entity("job");
//show_state();
}

public message    out( )
{

message   m = new message();
if (phase_is("busy")){
content   con = make_content("out",job);
m.add(con);
}
return m;
}

public void show_state()
{
System.out.println(   "state of  " +  name +  ": " );
System.out.println(  "phase, sigma,job_id : "
         + phase +  " " +  sigma +  " ");
        //       job.print();
  System.out.println( );
}


}

