/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  DemoDrawPanel.java
 *  Version    :  1.0
 *  Date       :  12-9-96
 */

package Zdevs;
import java.lang.*;
import java.awt.*;
import java.applet.*;
import java.io.*;
import Zdevs.Zcontainer.*;

public class DemoDrawPanel extends devsDrawPanel{
 // Choice menu;
 digExternalDialog digExternalDialog;
 // Button b;
 // TextField tf;
 protected Class cla;
 Panel globalPanel;
 digraph dig;
 set myCoupled;
 int min;
 clock cl;
 Button bstep, bstop, brest,btog,bhelp;
 Button    bext;
 Button bq;
 TextField tfe;
 boolean first = true;
 boolean stepMode = true;
 coord1 Coord;
 Param param = null;

 public DemoDrawPanel(){
 super();
 System.out.println("This is a test");
 }

 public DemoDrawPanel(devs d){
  super();
  //myDevs = d;
  System.out.println("This is a test ");
 }

 public void start(){
 }

 public void init(){
  getClassInstance();
  setLayout(new GridLayout(2, 3));//better than 3,2
  globalPanel = new Panel();
  add(globalPanel);
  addButtons();
  addCompPanels();
  validate();
  show();
  stepMode = !stepMode;
  System.out.println("STEPMODE " + stepMode);
  if(!stepMode){
    Coord = new coord1(dig,cl,1000,param,first);
    Coord.start();
  }
  else Coord.stopAtNextIter();
 }

 protected  void addButtons(){
  cl = new clock(0);
  globalPanel.add(cl.get_tf());
  globalPanel.add(new Button("Quit"));
  //bstop = new Button("Set Params");
  //globalPanel.add(bstop);
  bstep = new Button("Step ");
  globalPanel.add(bstep);
  //brest = new Button("Restart");
  //globalPanel.add(brest);
  btog = new Button("Toggle Step Mode");
  globalPanel.add(btog);
  //bext = new Button("Inject External");
  // globalPanel.add(bext);
  //   bhelp = new Button("Help");
  // globalPanel.add(bhelp);
 }

 protected  void addCompPanels(){
  myCoupled = dig.get_atomic_components();
  System.out.println("Components: ");
  myCoupled.print_all();
  for( entity p = myCoupled.get_head();p != null;p = p.get_right()){
     entity pp = p.get_ent();
     atomic1 A = (atomic1)pp;
     atomicDrawPanel ap = new atomicDrawPanel(A,false);
     add(ap);
     //System.out.print(" myCoupled ");
   }
  //tellAllPaint();
 }

 public void setTfe(int e){
  tfe.setText(Integer.toString(e));
 }

 protected  void getClassInstance(){
  try{
    String s =  "DemoLogistic";
    cla = Class.forName(s);
  }
  catch(Exception e){
    System.out.println("No such class " + e);
  }
  System.out.println("Class is " + cla.getName());
   try{
     Object o = cla.newInstance( );//need constructor with 0 args
     //e.g., atomTest()
     dig = (digraph)o;
   }
  catch(Exception e){
    System.out.println("can't create " + e);
  }
 }

 public boolean action(Event evt, Object obj){
  if(evt.target instanceof Button){
    if("Toggle Step Mode".equals(obj)){
      stepMode = !stepMode;
      System.out.println("STEPMODE " + stepMode);
       if(!stepMode){
         Coord = new coord1(dig,cl,1000,param,first);
         Coord.start();
       }
       else Coord.stopAtNextIter();
      return true;
    }
    if("Step ".equals(obj)){
      if(stepMode){
        Coord = new coord1(dig,cl,1,param,first);
        Coord.start();
        if(first)
          first = false;
      }
      return true;
    }
  if("Restart".equals(obj)){
    int inf = (new devs("a")).INFINITY;
    // System.out.println("inf = " + inf);
    if(!stepMode ||min >= inf )
      Coord.stopAtNextIter();
    tellAllInitialize();
    tellAllNow();
    cl.stop();
    if(!stepMode)
      Coord = new coord1(dig,cl,1000,true,param,true);
    else Coord = new coord1(dig,cl,1,true,param,true);
    Coord.start();
    first = false;
    return true;
  }
  }
 return false;
 }
}

