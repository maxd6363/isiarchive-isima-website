/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : atomicDrawPanel.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;
import java.lang.*;
import java.awt.*;
import java.applet.*;
import java.io.*;
import Zdevs.Zcontainer.*;

public class atomicDrawPanel extends Panel{
 protected devsDrawPanel myDevsVarDrawPanel;

 public void redraw(){
  graph.repaint();
  // graph.start_anim();  //vinc
 }

 public Graph graph;
 atomic1 myAtomic;
 Button badd,btest,bstep,bext,bhelp,bpar;

 public atomicDrawPanel(atomic1 a, boolean doButtons_, devsDrawPanel dp){
  super();
  myDevsVarDrawPanel = dp;
  if(!doButtons_){//in digraph
	myAtomic = a;
	myAtomic.setAtomicDrawPanel(this);
  }
  System.out.println("Atomic DrawPanel Started ");
  doButtons = doButtons_;
  init();
 }

 public atomicDrawPanel(atomic1 a){
  super();
  myAtomic = a;
  myAtomic.setAtomicDrawPanel(this);
  System.out.println("Atomic DrawPanel Started ");
  doButtons = true;
  init();
 }

 public atomicDrawPanel(atomic1 a, boolean doButtons_, boolean doProd_, devsDrawPanel dp){
  super();
  myDevsVarDrawPanel = dp;
  myAtomic = a;
  myAtomic.setAtomicDrawPanel(this);
  if(doProd_) myAtomic.setProd();
    System.out.println("Atomic DrawPanel Started ");
  doButtons = false;
  doProd = doProd_;
  init();
 }

 protected Choice menu;
 protected ExternalDialog externalDialog;
 protected boolean doButtons;
 protected boolean doProd = false;
 protected Panel buttons;
 protected Panel top;
 protected Button b;
 protected TextField tf;
 protected Class cl;
 protected Button bq;
 protected TextField tfe;
 protected Button bs;
 protected gui myGui;

 public void init(){
  //resize(300, 200);
  setLayout(new BorderLayout());
  top = new Panel();
  //top.resize(200, 50);
  top.setLayout(new BorderLayout());
  if(doButtons) {
    buttons = new Panel();
	buttons.setLayout(new FlowLayout());
    b =  new Button("Enter Atomic Class");
    buttons.add(b);
    menu = new Choice();
	try{
      cl = Class.forName("atoms");
    }

  catch(Exception e){
       System.out.println("No such class " + e);
  }
  try{
    Object o = cl.newInstance( );//need constructor with 0 args e.g., atomTest()
    list as = (list)o;
	for(int i = 0;i < as.get_length(); i++)
	   menu.addItem(as.list_ref(i).get_name());
  }
  catch(Exception e){
       System.out.println("can't create " + e);
  }
  buttons.add(menu);
  bq = new Button("Select message and e");
  tfe = new TextField("0",20);
  tfe.setEditable(true);
  top.add("North", buttons);
  }
  add("North", top);
  graph = new Graph(myAtomic, this);
  graph.init();
  add("Center", graph);
  if(!doButtons) {//digraph
    Panel bottom = new Panel();
    bottom.setLayout(new FlowLayout());
    bs = new Button("Show State");
    bottom.add(bs);
    bpar = new Button("Show Parameter");
    bottom.add(bpar);
    add("South",bottom);
 	bottom.resize(200,300);
    resize(200,300);
    validate();
    show();
  }
  System.out.println("Atomic DrawPanel Finished init");
 }

 public void signalOutput(){
  graph.SignalOutput(graph, myAtomic,
  myAtomic.make_content("",new entity()));
 }

 public void Now(){
  //graph.Now(); //BPZ should be able to add this in
 }

 public void start(){
  //myAtomic.start();
 }

 public void stop(){
  myAtomic.stop();
 }

 public void resume(){
  myAtomic.resume();
 }

 public void step(){
  myAtomic.step();
 }

 private  void getClassInstance(){
  try{
    String s = menu.getSelectedItem();
    //String s = tf.getText();
    cl = Class.forName(s);
    //buttons.remove(tf);
    buttons.remove(b);
    buttons.remove(menu);
  }
  catch(Exception e){
       System.out.println("No such class " + e);
  }

  System.out.println("Class is " + cl.getName());
  try{
     Object o = cl.newInstance( );//need constructor with 0 args e.g., atomTest()
     myAtomic = (atomic1)o;
     myAtomic.setAtomicDrawPanel(this);
     graph.setMyAtomic(myAtomic); //bpz
     graph.repaint();//bpz         // comment by Vinc
     //graph.start_anim(); //Vinc
     Panel bottom = new Panel();
	 bottom.setLayout(new FlowLayout());
     bs = new Button("Show State");
     bottom.add(bs);
     bpar = new Button("Show Parameter");
     bottom.add(bpar);
     add("South",bottom);
     bottom.resize(300,300);
     // resize(400,500);
     reshape(0,100,300,300);
     validate();
     show();
  }
  catch(Exception e){
     System.out.println("can't create " + e);

  }
 }

 private void addButtons(){
  top.add("Center", myAtomic.get_tf());
  badd = new Button("Toggle Step Mode");
  btest = new Button("Restart");
  bstep = new Button("Step");
  bext = new Button("Inject External");
  bhelp = new Button("Help");
  buttons.add(badd);
  buttons.add(bstep);
  buttons.add(bext);
  buttons.add(btest);
  buttons.add(bhelp);
  System.out.println("Atomic Panel Finished init");
  buttons.validate();
  resize(200,200);
 }

 public void setTfe(int e){
  tfe.setText(Integer.toString(e));
 }


 private  void queryExternal(){
  externalDialog = new ExternalDialog(this);
  externalDialog.add(bq);
  externalDialog.add(myAtomic.getL());
  externalDialog.add(tfe);
 }

 private  void getHelp(){
  helpPanel p = new helpPanel(this);
 }

 void getMessage(){
  //  myAtomic.stop();
  int e = new Integer(tfe.getText()).intValue();
  entity ee = myAtomic.getFn().assoc(myAtomic.getL().getSelectedItem());
  message m = (message)ee;
  //myAtomic.resume();
  myAtomic.inject(m,e);
 }

 private  void showState(){
  externalDialog = new ExternalDialog(this,200);
  externalDialog.add(myAtomic.getLL());
 }

 void removeQuery(){
  /*
  buttons.remove(tfe);
  buttons.remove(bq);
  buttons.remove(myAtomic.getL());
 */
 }

 public void delay(int del){
  Thread t = Thread.currentThread();
  try{
    t.sleep(del);
  }
  catch(Exception e){}
 }


 public void displayOutput(Panel panel,message m){
  java.awt.List li =  new java.awt.List(5, false);
  for(entity p = m.get_head();p != null;p = p.get_right()){
     entity ent = p.get_ent(); //since it is element
     content c =  (content)ent;
     li.addItem(c.p+" "+c.val.get_name());
  }
  panel.add(li);
  panel.validate();
  resize(400,400);
  delay(1000);
  panel.remove(li);
 }

 public boolean action(Event evt, Object obj){
  if(evt.target instanceof Button)
    if("Enter Atomic Class".equals(obj)){
      getClassInstance();
      addButtons();
      myAtomic.initialize();
      myAtomic.start();
    }
  if("Inject External".equals(obj)){
    queryExternal();
  }
  if("Help".equals(obj)){
    getHelp();
  }
  if("Show State".equals(obj)){
    showState();
  }
  if("Toggle Step Mode".equals(obj))
	myAtomic.toggleStepMode();
  if("Restart".equals(obj)){
	myAtomic.initialize();
	repaint();
	return true;
  }
  if("Step".equals(obj))
    myAtomic.step();
  if("Show Parameter".equals(obj)){
    myGui = new gui(this);
    myGui.addChoice();
  }
  if("Select Choice".equals(obj)){
    myGui.getInstance();
  }
  return false;
 }


 public list getInfoEnts(){
  return myAtomic.getInfoEnts();
 }

 public entity withName(String s){
    return myAtomic.withName(s);
 }

 public void addToEnd(infoEnt e){
  myAtomic.addToEnd(e);
 }

}
