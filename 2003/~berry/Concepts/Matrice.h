// Matrice.h
// D�finition de la classe Matrice

#include <iostream>

using namespace std;

#ifndef MATRICE_H
#define MATRICE_H

template <class Typ>class Matrice
{
 protected:
  int nbligne;
  int nbcol;
  Typ* data;

 public:
  Matrice();
  Matrice(int _nbligne, int _nbcol);
  Matrice(const Matrice& _matrice);
  int getNbLigne()const;
  int getNbCol()const;
  void operator=(const Matrice<Typ>& _matrice);
  void afficherMatrice()const;
  void setElement(int i,int j,Typ _elt);
  Typ getElement(int i,int j)const;
  void setTaille(int _nbligne,int _nbcol);
  void ajoutLigne(const Matrice<Typ>& ligne);
  void supprimLigne(int num);
  ~Matrice();
};

template<class Typ>Matrice<Typ>::Matrice(){
  nbligne=1;
  nbcol=1;
  data=new Typ[nbligne*nbcol];
}

template<class Typ>Matrice<Typ>::Matrice(int _nbligne,int _nbcol){
  nbligne=_nbligne;
  nbcol=_nbcol;
  data=new Typ[nbligne*nbcol];
}

template<class Typ>Matrice<Typ>::Matrice(const Matrice& _matrice){
  nbligne=_matrice.nbligne;
  nbcol=_matrice.nbcol;
  data=new Typ[nbligne*nbcol];
  for (int i=0;i<nbligne;i++)
    for (int j=0;j<nbcol;j++)
      data[i*nbcol+j]=_matrice.data[i*nbcol+j];
}

template<class Typ>int Matrice<Typ>::getNbLigne()const{
  return nbligne;
}

template<class Typ>int Matrice<Typ>::getNbCol()const{
  return nbcol;
}

template<class Typ>void Matrice<Typ>::operator=(const Matrice<Typ>& _matrice){
  delete[] data;
  nbligne=_matrice.nbligne;
  nbcol=_matrice.nbcol;
  data=new Typ[nbligne*nbcol];
  for (int i=0;i<nbligne;i++)
    for (int j=0;j<nbcol;j++)
      data[i*nbcol+j]=_matrice.data[i*nbcol+j];
}

template<class Typ>void Matrice<Typ>::afficherMatrice()const{
  if (nbligne==0) cout<<"Matrice vide"<<endl;
  else{
    for(int i=0;i<nbligne;i++){
      for(int j=0;j<nbcol;j++) cout<<getElement(i,j)<<"\t";
      cout<<endl;
    }
  }
}

// M�thode modifiant l'�l�ment (i,j) :
template<class Typ>void Matrice<Typ>::setElement(int i, int j,Typ _elt){
  data[i*nbcol+j]=_elt;
}

// M�thode retournant l'�l�ment (i,j) :
template <class Typ>Typ Matrice<Typ>::getElement(int i, int j)const{
  return data[i*nbcol+j];
}

// M�thode modifiant la taille de la matrice :
template<class Typ>void Matrice<Typ>::setTaille(int _nbligne,int _nbcol){
  delete[] data;
  nbligne=_nbligne;
  nbcol=_nbcol;
  data=new Typ[nbligne*nbcol];
}

// M�thode ajoutant une ligne a la matrice :
template<class Typ>void Matrice<Typ>::ajoutLigne(const Matrice<Typ>& ligne){
  Matrice<Typ> temp(*this);
  delete[] data;
  nbligne++;
  data=new Typ[nbligne*nbcol];
  for (int i=0;i<nbligne-1;i++)
    for (int j=0;j<nbcol;j++)
      data[i*nbcol+j]=temp.getElement(i,j);
  for (int j=0;j<nbcol;j++)
    this->setElement((nbligne-1),j,ligne.getElement(0,j));
}

// M�thode supprimant la ligne num de la matrice :
template<class Typ>void Matrice<Typ>::supprimLigne(int num){
  Matrice<Typ> temp(*this);
  delete[] data;
  nbligne--;
  int k=0;
  data=new Typ[nbligne*nbcol];
  for (int i=0;i<nbligne+1;i++){
    if (i!=num){
	  for (int j=0;j<nbcol;j++)
        this->setElement(k,j,temp.getElement(i,j));
	  k++;
    }
  }
}

template<class Typ>Matrice<Typ>::~Matrice(){
  delete[] data;
}

#endif
