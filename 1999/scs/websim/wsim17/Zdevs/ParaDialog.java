/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : paraDialog.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;
import java.awt.*;
import java.applet.*;


public class ParaDialog extends Dialog
{
 protected gui myGui;

 public boolean action(Event evt, Object obj) {
		if(evt.target instanceof Button)
		 if("Parameter".equals(obj))
	          myGui.addChoice();
		 if("Select Choice".equals(obj))
	      {
          myGui.getInstance();

          }
		 if("OK".equals(obj))
	      {
           myGui.getInfo();
           myGui.removeSelf();
          }


	return false;
	}



    public ParaDialog(gui myG)

    {
		super(new Frame(), "Set Parameter Values", false);
		resize(200, 200);
		setLayout(new FlowLayout());
        myGui = myG;
		show();
		Point p = Graph.AbsLocation(myGui);
		move(p.x+ 100 , p.y+ 500);
		//show();
    }


}