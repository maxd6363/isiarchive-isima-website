# LaTeX2HTML 96.1 (Feb 5, 1996)
# Associate sections original text with physical files.

$key = q/0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '0%:%websim.html%:%<B>Toward a Web-Based Modeling Methodology and Repository</B>' unless ($section_info{$key}); 
$done{"websim.html"} = 1;
$key = q/0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node1.html%:%ABSTRACT' unless ($section_info{$key}); 
$done{"node1.html"} = 1;
$key = q/0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node2.html%:%INTRODUCTION' unless ($section_info{$key}); 
$done{"node2.html"} = 1;
$key = q/0 0 0 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node3.html%:%MOOSE' unless ($section_info{$key}); 
$done{"node3.html"} = 1;
$key = q/0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node4.html%:%FUTURE WORK' unless ($section_info{$key}); 
$done{"node4.html"} = 1;
$key = q/0 0 0 5 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node5.html%:%ACKNOWLEDGMENTS' unless ($section_info{$key}); 
$done{"node5.html"} = 1;
$key = q/0 0 0 6 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node6.html%:%REFERENCES' unless ($section_info{$key}); 
$done{"node6.html"} = 1;
$key = q/0 0 0 7 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node7.html%:%References' unless ($section_info{$key}); 
$done{"node7.html"} = 1;
$key = q/0 0 0 8 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node8.html%:%AUTHOR BIOGRAPHY' unless ($section_info{$key}); 
$done{"node8.html"} = 1;
$key = q/0 0 0 9 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/;
$section_info{$key} = '3%:%node9.html%:%  About this document ... ' unless ($section_info{$key}); 
$done{"node9.html"} = 1;

1;

