/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  couppr.java
 *  Version    :  1.0
 *  Date       :  12-9-96
 */

package Zdevs;
import Zdevs.Zcontainer.*;


public class couppr extends pair{

 public entity get_devs(){
  return get_key();
 }

 public port get_port(){
  return ((port )get_value());
 }

 public couppr(entity c,port p){
  super( c,p );
 }
}

