/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : atomic.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;
import Zdevs.Zcontainer.*;
import java.awt.Panel;


public class atomic extends atomicbase {
 protected atomic addSibC;

 public void addSibCoupling(atomic a, String p1, String p2){
  addSibC = a;
  addSibCoup(p1, p2);
 }

 public void remove_coupling(devs d1, String p1, devs d2, String p2){
  if(((digraph)parent) == null)
    return;
  ((digraph)parent).remove_coupling(d1,p1,d2,p2);
 }

 public void removeSibCoupling(atomic a,String myoutPort,String sibInport){
  removeSibling(a);
  remove_coupling(this,myoutPort,a,sibInport);
 }

 public void addSibCoup(String myoutPort,String sibInport){
  addSibling(addSibC);
  add_coupling(this,myoutPort,addSibC,sibInport);
 }

 public void  add_coupling(devs d1, String p1, devs d2, String p2){
  if(((digraph)parent) == null)
    return;
  ((digraph)parent).add_coupling(d1,p1,d2,p2);
 }

 public void addPhases(){} //for adding phases

 public void initialize(){
  super.initialize();
  addPhases();
  }

 public void injectPeer( atomic A,String phase,entity val,int e){
  A.inject(phase,val,tL + e);
  A.myRepaint();
 }

 public void injectPeer( atomic A,String phase,entity val){
  A.inject(phase,val,tL);
  A.myRepaint();
 }

 public void removeSibling(atomic A){
  if(((digraph)parent) == null)
    return;
  if(!((digraph)parent).get_components().is_in(A))
    return;
  atomicDrawPanel ad = GetAtomicDrawPanel();
  atomicDrawPanel ap = (atomicDrawPanel)ad;
  devsDrawPanel dp = ap.myDevsVarDrawPanel;
  // DemoDrawPanel dp = ap.myDevsVarDrawPanel;
  ((digraph)parent).remove(A);
  atomicDrawPanel adA = A.GetAtomicDrawPanel();
  atomicDrawPanel apA = (atomicDrawPanel)adA;
  dp.remove(apA);
  dp.validate();
  dp.show();
  //dp.tellAllPaint();
 }

 public void addSibling(atomic A){
  atomicDrawPanel apA;
  if(((digraph)parent) == null)
    return;
  atomicDrawPanel ap = GetAtomicDrawPanel();
  devsDrawPanel dp = ap.myDevsVarDrawPanel;
  if((((digraph)parent).get_components()).is_in(A)){
    apA = A.GetAtomicDrawPanel();
  }
  else{ // A is new
      A.set_tL(tL+sigma);
      ((digraph)parent).add(A);
      ((digraph)parent).reInitialize(A);
      A.setCoord(new coord1()); //to get it to runInDigraph
      //A.start(); //since coord will have startedup the others
      apA = new atomicDrawPanel(A,false,dp);
      dp.add(apA);
  }
  apA.reshape(getLocationX()+50,getLocationY()+150,200,200);
  dp.validate();
  dp.show();
 }

 protected set phases;
 protected message lastOutput;

 public atomic(String name){
  super(name);
  phases = new set();
  lastOutput = new message();
  inports.add("in");
  outports.add("out");
  phases.add("passive");
  passivate();
 }

 public message getLastOutput(){
  return lastOutput;
 }

 public String getPhase(){
  return phase;
 }

 public void passivate(){
  phase = "passive";
  sigma = INFINITY;
 }

 public void passivate_in(String phase){
  hold_in(phase, INFINITY);
 }

 public boolean phase_is(String  Phase){
  if(!(phases.is_in_name(Phase))) {
    System.out.println( "Warning: model: " + get_name() + " phase: " + Phase + " has not been declared" );

  }
  return phase.equals(Phase);

   /*
  try{
    phases.is_in_name(Phase);
     }
   catch (Exception e)
   {
   return phase.equals(Phase);
   }
   return false;
   */
 }


 public void show_state(){
  System.out.println( "model " + name +": "+ "phase " + phase + ", sigma " + sigma );
 }


// DEFINITELY NEED THIS HERE :: inheritance doesn't work

 public void  wrap_deltfunc(int t,message x){
  //System.out.println("in wrap_deltfunc");
  //System.out.println("model " + get_name() + " x = ");
  //if(x != null) x.print_all();System.out.println();
  for(entity p = x.get_head();p != null;p = p.get_right()) {
      entity ent = p.get_ent(); //since it is element
      content c =  (content)ent;
     //code that calls for an OutputBox to go from the source to this
	 atomic source = (atomic)c.source;
     atomicDrawPanel myap = GetAtomicDrawPanel();
     atomicDrawPanel sap = source.GetAtomicDrawPanel();
     //to avoid interference with atomDigraph
     if(myap != null && sap != myap && Prod == false)
       myap.graph.SignalOutput(sap.graph,source,c);
  }
  if(x.empty() && t != tN){
    return;
  }
  else if((!x.empty()) && t == tN){
        int e = t - tL;
        deltcon(e,x);
  }
  else if(t == tN){
    if(modelType.equals("atomic"))//prevent output function being called twice
      lastOutput = out();
    deltint();
  }
  else if(!x.empty()){
        int e = t - tL;
        deltext(e,x);
  }
  tL = t;
  tN = tL + ta();

 }

 public void deltcon(int e,message x){
  deltint();
  deltext(e,x);
 }

 public void hold_in(String p, int s){
  phase = p;
  sigma = s;
 }

 public void simulate( int num_iter){
  int i=1;
  while( (tN < INFINITY) &&(i<=num_iter)){
      compute_input_output(tN);
      wrap_deltfunc(tN, new message());
      i++;
  }
 }
}
