




1)Intitul� du poste: DEVELOPPEMENT FPGA POUR UN ECARTOMETRE

R�f�rence : MSY/URD09/VBVT/07 

Entreprise : SAGEM SA

Descriptif du poste : SUJET REF. MSY/URD09/VBVT/07

NIVEAU : INGENIEUR 2�me ou 3�me ann�e ELECTRONIQUE

DUREE : 6 mois d�s que possible

DESCRIPTIF DU SUJET :
Le syst�me SAS 90 a �t� d�velopp� afin d'automatiser le pointage de canons terrestres anti-a�riens sur des cibles volantes. Ce syst�me comporte entre autres un �cartom�tre, c'est-�-dire un module de traitement d'images permettant de situer la cible dans l'image acquise par une cam�ra du syst�me. Or, SAS 90 ayant �t� con�u dans les ann�es 1990, deux cartes et plusieurs composants programmables aujourd'hui frapp�s d'obsolescence ont �t� n�cessaires pour r�aliser cet �cartom�tre. Nous souhaiterions le moderniser en le r�implantant sur une carte unique avec un seul composant programmable (typiquement une matrice FPGA Xilinx). Ce stage se d�cline de la fa�on suivante :
- 2 semaines : prise en main du projet, �tude des 2 cartes actuelles,
- 3 mois : �tude des composants programmables, r�daction d'une sp�cification de besoin technique du FPGA,
- 2,5 mois : codage VHDL du nouveau FPGA et simulation fonctionnelle,
- s'il reste du temps : d�but de l'�tude de la nouvelle carte qui accueillera le FPGA;

CONNAISSANCES INDISPENSABLES : Programmation VHDL

CONTACT :
e-mail : claire.cerveau@sagem.com

PAR COURRIER :
SAGEM SA
A l'attention de Claire CERVEAU
178 Rue de Paris
91344 MASSY CEDEX

P�riode : DES QUE POSSIBLE

Fonction/Secteur : Electronique-Optique-Electricit�

Zone g�ographique : MASSY

Contact : claire.cerveau@sagem.com

Lien vers le site 
___________________________________________________________________________
2)Intitul� du poste: BILAN ET SIMULATION ALGORITHME CONDUITE DE TIR

R�f�rence : MSY/URD09/VBVT 08 et 09 

Entreprise : SAGEM SA

Descriptif du poste : SUJET REF. MSY/URD09/VBVT/08 ET 09

NIVEAU : 3�me ann�e INGENIEUR INFORMATIQUE INDUSTRIELLE/AUTOMATIQUE

DUREE : 4 mois minimum

DESCRIPTIF DU SUJET :
Le pr�sent sujet de fin d'�tudes a pour objet le d�veloppement d'un logiciel embarqu� optimis� de conduite de tir pour un syst�me de vis�e et des outils de traitement associ�s. Ce projet de fin d'�tudes comportera les �tapes suivantes :
- analyse des acquis existants et identification des modules r�utilisables,
- sp�cification des modules logiciels embarqu�s et des outils de traitement compl�mentaires sur environnement PC,
- validation sur syst�me de vis�e.

CONNAISSANCES SOUHAITABLES :
Environnement PC bas niveau,
Windows 2000, langage C, informatique embarqu�e et temps r�el.

CONTACT :
e-mail : claire.cerveau@sagem.com

COURRIER :
SAGEM SA
A l'attention de Claire CERVEAU
178 Rue de Paris
91344 MASSY CEDEX

P�riode : 2�me trimestre 2003

Fonction/Secteur : Informatique-Math�matique-Telecom

Zone g�ographique : MASSY

Contact : claire.cerveau@sagem.com

Lien vers le site 
________________________________________________________________________
3)Intitul� du poste: DEVELOPPEMENT BAIE DE MISE EN OEUVRE VISEUR OPTRONIQUE

R�f�rence : MSY/URD09/VBVT/10 

Entreprise : SAGEM SA

Descriptif du poste : SUJET REF. MSY/URD09/VBVT/10

NIVEAU : 2�me ann�e �cole d'ing�nieur informatique industrielle

DUREE : 3 mois minimum

DESCRIPTIF DU SUJET :
Le pr�sent sujet de stage a pour objet la r�alisation d'une baie de mise en oeuvre d'un syst�me de vis�e. Cette baie de mise en oeuvre sera bas�e sur un ordinateur portable PC fonctionnant dans un environnement Windows 2000.

Cette baie de mise en oeuvre doit permettre les fonctions suivantes :
- t�l�chargement des diff�rents logiciels embarqu�s du syst�me de vis�e,
- pilotage � distance des diff�rents organes du syst�me de vis�e (modes et consignes) et r�cup�ration de l'�tat des diff�rents organes,
- transfert et exploitation des informations sauvegard�es en temps r�el dans le syst�me de vis�e,
- aide � la maintenance du bo�tier �lectronique du syst�me de vis�e, par l'envoi de stimulis appropri�s � chaque carte (cartes en sortie) et r�cup�ration des donn�es acquises (cartes en entr�e).

CONNAISSANCES SOUHAITABLES :
Environnement PC bas niveau,
Windows2000, Langage C, informatique embarqu�e et temps r�el.

CONTACT :
e-mail : claire.cerveau@sagem.com

COURRIER :
SAGEM SA
A l'attention de Claire CERVEAU
178 Rue de Paris
91344 MASSY CEDEX

P�riode : 2�me ou 3�me trimestre 2003

Fonction/Secteur : Informatique-Math�matique-Telecom

Zone g�ographique : MASSY

Contact : claire.cerveau@sagem.com

Lien vers le site 
___________________________________________________________________________________
4)Intitul� du poste: REALISATION D'UN MODELE DE PORTEE DE VOIE OPTRONIQUE A CAPTEUR TV

R�f�rence : MSY/URD09/VBVT/13 

Entreprise : SAGEM SA

Descriptif du poste : SUJET REF. MSY/URD09/VBVT/13

NIVEAU : Derni�re ann�e �cole d'ing�nieur

OPTION : Traitement du signal, optronique

DUREE : 3 mois d�s que possible

DESCRIPTIF DU SUJET :
- A partir d'une prise en compte des mod�les d'�valuation de port�es de d�tection, de reconnaissance et d'identification de voies optroniques TV � capteur CCD existants, de leur comparaison, d'un bilan de l'exp�rience acquise et d'une revue de demandes des clients, une synth�se sera effectu�e donnant lieu � la r�daction d'une sp�cification de besoin.

- Un outil de mod�lisation informatique sera r�alis� pour r�pondre � la sp�cification ci-dessus. Il devra prendre en compte le rapport signal � bruit, les types de visualisation et de standards et comporter des options pour s'adapter au niveau cognitif des donn�es d'entr�e ainsi qu'une possibilit� d'analyse param�trique de sensibilit�.

- Les algorithmes utilis�s dans le mod�le feront l'objet un Dossier Justificatif et une validation par des essais r�els non exhaustifs est pr�vue en final.

CONNAISSANCES SOUHAITABLES :
EXCEL, MATLAB, LABVIEW...
Optronique (traitement du signal, FTM, d�tection, visualisation, aspect physiologique de l'analyse d'image...)

CONTACT :
e-mail : claire.cerveau@sagem.com

COURRIER :
SAGEM SA
A l'attention de Claire CERVEAU
178 Rue de Paris
91344 MASSY CEDEX

P�riode : DES QUE POSSIBLE

Fonction/Secteur : Electronique-Optique-Electricit�

Zone g�ographique : MASSY

Contact : claire.cerveau@sagem.com

Lien vers le site 
__________________________________________________________________________________
5)Intitul� du poste: REALISATION D'UN MODELE DE PORTEE DE VOIE OPTRONIQUE A CAPTEUR TV

R�f�rence : MSY/URD09/VBVT/13 

Entreprise : SAGEM SA

Descriptif du poste : SUJET REF. MSY/URD09/VBVT/13

NIVEAU : Derni�re ann�e �cole d'ing�nieur

OPTION : Traitement du signal, optronique

DUREE : 3 mois d�s que possible

DESCRIPTIF DU SUJET :
- A partir d'une prise en compte des mod�les d'�valuation de port�es de d�tection, de reconnaissance et d'identification de voies optroniques TV � capteur CCD existants, de leur comparaison, d'un bilan de l'exp�rience acquise et d'une revue de demandes des clients, une synth�se sera effectu�e donnant lieu � la r�daction d'une sp�cification de besoin.

- Un outil de mod�lisation informatique sera r�alis� pour r�pondre � la sp�cification ci-dessus. Il devra prendre en compte le rapport signal � bruit, les types de visualisation et de standards et comporter des options pour s'adapter au niveau cognitif des donn�es d'entr�e ainsi qu'une possibilit� d'analyse param�trique de sensibilit�.

- Les algorithmes utilis�s dans le mod�le feront l'objet un Dossier Justificatif et une validation par des essais r�els non exhaustifs est pr�vue en final.

CONNAISSANCES SOUHAITABLES :
EXCEL, MATLAB, LABVIEW...
Optronique (traitement du signal, FTM, d�tection, visualisation, aspect physiologique de l'analyse d'image...)

CONTACT :
e-mail : claire.cerveau@sagem.com

COURRIER :
SAGEM SA
A l'attention de Claire CERVEAU
178 Rue de Paris
91344 MASSY CEDEX

P�riode : DES QUE POSSIBLE

Fonction/Secteur : Informatique-Math�matique-Telecom

Zone g�ographique : MASSY

Contact : claire.cerveau@sagem.com

Lien vers le site 
_________________________________________________________________________________________
6)Intitul� du poste: REALISATION D'UN DEMONSTRATEUR DES TECHNOLOGIES SAGEM DANS LE DOMAINE DES COMMANDES DE VOL

R�f�rence : MSY/02-03/URD21/004 

Entreprise : SAGEM SA

Descriptif du poste : SUJET REF. MSY/02-03/URD21/004

NIVEAU : DERNIERE ANNEE INGENIEUR AERONAUTIQUE

DUREE : 4 mois d�s que possible

DESCRIPTIF DU SUJET :
L'objet du stage est de r�aliser une maquette de d�monstration des �l�ments de commandes de vol �lectriques et de les int�grer dans un simulateur de vol.
Il faudra interfacer les modules commandant les axes lacet, roulis et tangage (� instrumenter en partie), la manette de gaz et d'autres �quipements avec un logiciel du type Flight Simulator.
La r�alisation de ce projet est confi�e � un bin�me.
La maquette sera bas�e sur le banc de test actuel du syst�me et devra prendre en compte des aspects esth�tiques, afin de pouvoir la pr�senter dans un show-room ou lors de salons.

CONNAISSANCES SOUHAITABLES :
Bonnes connaissances de l'�lectronique et de la programmation,
Anglais souhaitable
Autonomie et pluridisciplinaire.

PRIORITE : URGENT

CONTACT :
e-mail : claire.cerveau@sagem.com

COURRIER :
SAGEM SA
A l'attention de Claire CERVEAU
178 Rue de Paris
91344 MASSY CEDEX

P�riode : URGENT

Fonction/Secteur : Electronique-Optique-Electricit�

Zone g�ographique : MASSY

Contact : claire.cerveau@sagem.com

Lien vers le site 
________________________________________________________________________________________
7)Intitul� du poste: DEVELOPPEMENT IHM PAMS

R�f�rence : MSY/02-03/URD21/006 

Entreprise : SAGEM SA

Descriptif du poste : SUJET REF. MSY/02-03/URD21/006

NIVEAU : Derni�re ann�e ing�nieur MS Informatique industrielle

DUREE : 3 mois (ASAP)

DESCRIPTIF DU SUJET :
- D�veloppement de l'interface IHM sur un �cran LCD avec Dalle tactile (logiciel class� de s�ret� pour l'interface op�rateur d'une centrale nucl�aire)

- D�veloppement d'une biblioth�que graphique sur PC � partir de commande graphique de base

- Validation de cette biblioth�que graphique par rapport aux aspects fonctionnels, ergonomiques et s�ret�.

- Prototype d'une IHM minimum

- Mesure des temps de r�ponse affichage dans les diff�rents cas IHM

Remarque : le LCD/Tactile est "interfac�" avec un bo�tier VCM d�velopp� par SAGEM. Ce bo�tier permet de transformer des commandes re�ues (comme un terminal graphique intelligent) sur une liaison s�rie en des donn�es affichables sur l'�cran.

CONNAISSANCES SOUHAITABLES : Langage C sous Borland

CONTACT :
e-mail : claire.cerveau@sagem.com

COURRIER :
SAGEM SA
A l'attention de Claire CERVEAU
178 Rue de Paris
91344 MASSY CEDEX

P�riode : DES QUE POSSIBLE

Fonction/Secteur : Informatique-Math�matique-Telecom

Zone g�ographique : MASSY

Contact : claire.cerveau@sagem.com

Lien vers le site 

