CALL FOR PAPERS
Special issue of Discrete Mathematics

In order to help  promote the emerging field of minimal separation, 
a special issue of DISCRETE MATHEMATICS will be dedicated to minimal 
separation and minimal triangulation.

SCOPE

This special issue will focus on minimal vertex separation in finite 
undirected graphs, its theoretical aspects, its importance in 
defining graph properties, and its application to special graph 
classes; papers dealing with  graph classes which can be shown to 
have a polynomial number of minimal separators are of particular 
interest.

We are also interested in the related issue of minimal triangulation, 
(i.e. minimal chordal completion), and in innovative results on 
embedding an arbitrary graph in classes with few minimal separators, 
especially if the embedding process is shown to be related to 
minimal separation as is the case for minimal triangulation.
('Embedding' can be done by adding edges  to the graph, but 
processes which remove edges can also be investigated).


SUBMISSION GUIDELINES

The deadline for submission is January 7, 2004.

Authors are expected to follow the general instructions to contributors 
for Discrete Mathematics.  

The manuscript should be submitted in electronic form (postscript or pdf) 
to berry@isima.fr,  with subject line "SUBMISSION for DISCRETE MATHEMATICS",  
with a copy to Jean.Blair@usma.edu.

Submissions will be acknowledged once the papers have been successfully 
printed or viewed; if you do not receive an acknowledgement within 
three days, please resend your submission.

In case there is no possibility for  electronic submission, send 3 hard 
copies of the manuscript to:
Anne Berry
LIMOS
Bat ISIMA-D123
BP 10 125
63 173 AUBIERE Cedex
FRANCE
to arrive before  January 7, 2004.


REVIEWING PROCESS

The volume will consist of a collection of thoroughly refereed research 
and survey papers. The review process will be conducted in the same manner 
as it is for regular submissions to Discrete Mathematics; papers will 
be evaluated based on the quality and originality of the contribution. 
Submission of a manuscript is a representation that the paper has neither 
been published in full version nor submitted for publication elsewhere. 
If an abstract or extended abstract of this paper  has been published, 
this should be written explicitly on a footnote in the title page.
 

GUEST EDITORS

Dr. Anne BERRY
LIMOS
Universite Blaise Pascal
Bat ISIMA-D123
BP 10 125
63 173 AUBIERE Cedex
FRANCE
email: berry@isima.fr
tel: (00 33) 4 73 40 77 69
fax:  (00 33) 4 73 40 50 01

Prof. Jean R. S. BLAIR
Department of Electrical Engineering and Computer Science
United States Military Academy
West Point, New York 10996-1787
U.S.A.
email: Jean.Blair@usma.edu
Phone: (845) 938-5003
Fax: (845) 938-3214

Dr. Genevieve SIMONET
LIRMM
CC 411
Departement Informatique
Bat K
IUT de Montpellier
99, avenue d'Occitanie
34296 MONTPELLIER cedex 5
FRANCE
email: simonet@iutmontp.univ-montp2.fr
Phone: (00 33) 4 99 58 51 82
Fax: (00 33) 4 99 58 51 76
