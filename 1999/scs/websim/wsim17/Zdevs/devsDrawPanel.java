/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : devsDrawPanel.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;
import java.lang.*;
import java.awt.*;
import java.applet.*;
import java.io.*;
import Zdevs.Zcontainer.*;

public class devsDrawPanel extends Applet{

    public   void removeCompPanels()
    {


    myCoupled = dig.get_atomic_components();

 for ( entity p = myCoupled.get_head();p != null;p = p.get_right()) {

       entity pp = p.get_ent();
        atomic1 A = (atomic1)pp;
       atomicDrawPanel ap = A.GetAtomicDrawPanel();
        remove(ap);

 }
}

    public digraph getMyDig()
    {
        return dig;
    }

    public void tellAllPaint()
  {
   for (entity p = myCoupled.get_head();p != null;p = p.get_right()) {
          entity ent = p.get_ent();
         atomic1 a = (atomic1)ent;

             a.GetAtomicDrawPanel().graph.setMyAtomic(a); //bpz
            a.GetAtomicDrawPanel().graph.repaint();//bpz


   }
}

    Choice menu;
digExternalDialog digExternalDialog;
    Button b;
    TextField tf;
    protected Class cla;
    Panel globalPanel;

  //  devs myDevs;
    digraph dig;
   protected set myCoupled;

    int min;
    clock cl;
    Button bstep, bstop, brest,btog,bhelp;
  Button    bext;
  Button bq;
  TextField tfe;

    boolean first = true;
    boolean stepMode = true;
    coord1 Coord;
    Param param = null;

    public devsDrawPanel(){
        super();
           System.out.println("This is a test");
    }


    public devsDrawPanel(devs d){
        super();
     //   myDevs = d;
        System.out.println("This is a test ");
    }

public void start(){

   }

public void init(){

		menu = new Choice();
			 try{

      cla = Class.forName("digraphs");
  }

  catch (Exception e){
          System.out.println
           ("No such class " + e);
  }
   try {
  Object o = cla.newInstance( );//need constructor with 0 args
                               //e.g., atomTest()

		list as = (list)o;
		for (int i = 0;i < as.get_length(); i++)
		   menu.addItem(as.list_ref(i).get_name());
		     }

  catch (Exception e){
          System.out.println
           ("can't create " + e);
  }


		add(menu);

	   b =  new Button("Enter Digraph Class");
       add(b);

bq = new Button("Select message and e");
tfe = new TextField("0",20);
tfe.setEditable(true);

  System.out.println("DevsDrawPanel initialized ");
}

protected void addButtons(){
     globalPanel = new Panel();
     cl = new clock(0);
     globalPanel.add(cl.get_tf());
     globalPanel.add(new Button("Quit"));
    bstop = new Button("Set Params");
   globalPanel.add(bstop);
        bstep = new Button("Step ");
   globalPanel.add(bstep);
    brest = new Button("Restart");
   globalPanel.add(brest);
    btog = new Button("Toggle Step Mode");
    globalPanel.add(btog);
  bext = new Button("Inject External");
  globalPanel.add(bext);
    bhelp = new Button("Help");
  globalPanel.add(bhelp);

}

protected   void addCompPanels(){

    myCoupled = dig.get_atomic_components();
   System.out.println("Components: ");

    myCoupled.print_all();


 for ( entity p = myCoupled.get_head();p != null;p = p.get_right()) {

       entity pp = p.get_ent();
        atomic1 A = (atomic1)pp;

     atomicDrawPanel ap = new atomicDrawPanel(A,false,this);
        add(ap);


  //System.out.print(" myCoupled ");
 }
tellAllPaint();
}
public void setTfe(int e){
    tfe.setText(Integer.toString(e));
}

protected  void queryExternal()
{
	digExternalDialog = new digExternalDialog(this);
 digExternalDialog.add(bq);
 digExternalDialog.add(dig.getL());
 digExternalDialog.add(tfe);
}

protected void getHelp()
{
	devsHelpPanel p = new devsHelpPanel(this);
}

public void  getMessage(){
int e = new Integer(tfe.getText()).intValue();
//always inject at 0 for now
 entity ee = dig.getFn().assoc
          (dig.getL().getSelectedItem());
 message m = (message)ee;
dig.wrap_deltfunc(0,m);
}

protected  void getClassInstance(){
    try{
//String s = tf.getText();
  String s = menu.getSelectedItem();
      cla = Class.forName(s);
      	remove(menu);
 //     remove(tf);
      remove(b);
  }

  catch (Exception e){
          System.out.println
           ("No such class " + e);
  }
  System.out.println("Class is " + cla.getName());
   try {
  Object o = cla.newInstance( );//need constructor with 0 args
                               //e.g., atomTest()
   dig = (digraph)o;

   }

  catch (Exception e){
          System.out.println
           ("can't create " + e);

  }
}



 public boolean action(Event evt, Object obj) {
	if(evt.target instanceof Button)
		{
		 if("Enter Digraph Class".equals(obj))
	      {
          getClassInstance();
          resize(750, 500);
		setLayout(new GridLayout(2, 3));//better than 3,2
        addButtons();
        add(globalPanel);
        addCompPanels();
        validate();
          show();

          }
  if("Help".equals(obj))
       {
           getHelp();
        }
        if("Quit".equals(obj))
        {
            tellAllKill();
        }
 if("Inject External".equals(obj))
                {
           queryExternal();
                }

	     if("Set Params".equals(obj))
             {
    if (param == null) param = new Param(dig.getButtons());

  /*
     System.out.println("Stop All ");
     tellAllStop();
     cl.stop();


    for (int i = 0; i< countComponents();i++){
              Component c = getComponent(i);
              atomicPanel a = (atomicPanel)c;
              a.stop();
    } //doesn't work
    */
 return true;
                }
         if("Toggle Step Mode".equals(obj))
                {
                 stepMode = !stepMode;
       System.out.println("STEPMODE " + stepMode);
         if  (!stepMode){
       Coord = new coord1(dig,cl,1000,param,first);
        Coord.start();
        }
         else Coord.stopAtNextIter();

                 return true;
                }
  if("Step ".equals(obj))               {

       if  (stepMode){

       Coord = new coord1(dig,cl,1,param,first);

       Coord.start();
       if (first) first = false;

       }
 return true;
                }

    if("Restart".equals(obj))
                {
   int inf = (new devs("a")).INFINITY;
  // System.out.println("inf = " + inf);
     if  (!stepMode ||min >= inf )
            Coord.stopAtNextIter();
       tellAllInitialize();
           tellAllNow();
           cl.stop();
   if (!stepMode) Coord = new coord1(dig,cl,1000,true,param,true);
   else Coord = new coord1(dig,cl,1,true,param,true);
       Coord.start();
   first = false;
 return true;
                   }

}

return false;
}

public void tellAllNow(){
   for (entity p = myCoupled.get_head();p != null;p = p.get_right()) {
          entity ent = p.get_ent();
         atomic1 a = (atomic1)ent;
             a.Now();


   }
}

public void tellAllKill(){
   for (entity p = myCoupled.get_head();p != null;p = p.get_right()) {
          entity ent = p.get_ent();
         atomic1 a = (atomic1)ent;
             a.kill();


   }
}

public void tellAllStop(){
   for (entity p = myCoupled.get_head();p != null;p = p.get_right()) {
          entity ent = p.get_ent();
         atomic1 a = (atomic1)ent;
             a.stop();


   }
}
public void tellAllInitialize(){
   for (entity p = myCoupled.get_head();p != null;p = p.get_right()) {
          entity ent = p.get_ent();
         atomic1 a = (atomic1)ent;
             a.initialize();

   }
}

public void tellAllStartup(){
   for (entity p = myCoupled.get_head();p != null;p = p.get_right()) {
          entity ent = p.get_ent();
         atomic1 a = (atomic1)ent;
             a.start(); // start them up

   }
}


public void tellAllResume(){
   for (entity p = myCoupled.get_head();p != null;p = p.get_right()) {
          entity ent = p.get_ent();
         atomic1 a = (atomic1)ent;
             a.resume();


   }
}
public void askAlltN(){
    min = 10000000;
   for (entity p = myCoupled.get_head();p != null;p = p.get_right()) {
          entity ent = p.get_ent();
         atomic1 a = (atomic1)ent;
           if (a.next_tN() < min) min = a.next_tN();

   }

}

public void tellAlltN(int t){
       for (entity p = myCoupled.get_head();p != null;p = p.get_right()) {
          entity ent = p.get_ent();
         atomic1 a = (atomic1)ent;
           a.setMin(t);

   }
}

public void tellAllStep(){
   for (entity p = myCoupled.get_head();p != null;p = p.get_right()) {
         entity ent = p.get_ent();
         atomic1 a = (atomic1)ent;
             a.step();


   }
}


}

