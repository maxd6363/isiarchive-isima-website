<!-- 

$(document).ready(function()
{

    // On s�lectionne tous les items de liste portant la classe "toggleSubMenu" 
    // et on remplace l'�l�ment span qu'ils contiennent par un lien : 
    $(".navigation li.toggleSubMenu span").each( function () { 
        // On stocke le contenu du span : 
        var TexteSpan = $(this).text(); 
        $(this).replaceWith('<a href="" title="Afficher le sous-menu">' + TexteSpan + '<\/a>') ; 
    } ) ; 
 
    // quelques coins ronds
    $("#ZZheader").corner("tr");
    $("#ZZfooter").corner("bottom");
    
    // On cache les sous-menus 
    // sauf celui qui porte la classe "open_at_load" : PAS ENCORE FAIT 
    $(".navigation ul.subMenu:not(.openatload)").hide();
    
    $(".navigation li.toggleSubMenu >a").click( function () { 
        // Si le sous-menu �tait d�j� ouvert, on le referme : 
        if ($(this).next("ul.subMenu:visible").length != 0) { 
            $(this).next("ul.subMenu").slideUp("normal", function () { $(this).parent().removeClass("open") } ); 
        } 
        // Si le sous-menu est cach�, on ferme les autres et on l'affiche : 
        else { 
            $(".navigation ul.subMenu").slideUp("normal", function () { $(this).parent().removeClass("open") }); 
            $(this).next("ul.subMenu").slideDown("normal", function () { $(this).parent().addClass("open") } ); 
        } 
        // On emp�che le navigateur de suivre le lien : 
        return false; 
    }); 
    
    $("#slider").easySlider({
				auto: true, 
				continuous: true,
                pause:4500,
				numeric: true
                
	});
    
	$('#partenaires').Fisheye(
				{
					maxWidth: 120,
					items: 'a',
					itemsText: 'span',
					container: '.fisheyeContainer',
					itemWidth: 32,
					proximity: 80,
					alignment : 'left',
					valign: 'bottom',
					halign : 'center'
				});
	
    //$('#photos').galleryView({
	//		filmstrip_size: 4,
	//		frame_width: 100,
	//		frame_height: 100,
	//		show_panels: false,
    //        show_filmstrip:true,
	//		show_captions:true
	//});

    
  
});

// --> 