/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  message.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs;
import Zdevs.Zcontainer.*;

public class message extends bag{

 public message(){
  super();
 }

 public content read(int i){ //avoid conflict
  if(i > get_length()){
    System.out.println( "index not in message length: " + i +" " + get_length());
    return null;
  }
  else{
    int cnt = 0;
    for(entity p = get_head();p != null;p = p.get_right()){
      if(cnt++ == i){
        // System.out.println(" good out1");
        entity ent = p.get_ent(); //since it is element
        content c =  (content)ent;
        return c;
      }
    }
    System.out.println(" null out1");
    return null;
  }
 }


 public void UNI(message m){
  if(!m.empty()){
    entity p1;
    for(entity p = m.get_head();p != null;p = p1){
       p1 = p.get_right();
       super.ADD(p);
    }
  }
 }

 public void uni(message m){
  if(!m.empty()) {
    entity p1;
    for(entity p = m.get_head();p != null;p = p1){
       p1 = p.get_right();
       super.add(p);
    }
  }
 }

 public boolean on_port(String portName, int i){
  content con = read(i);
  return portName.equals(con.p);
 }

 public entity get_val_on_port(String portName, int i){
  if(on_port(portName,i)){
    return read(i).val;
  }
  return null;
 }

 public addrclass  get_address_on_port(String portName, int i){
  if(on_port(portName,i))
    return read(i).address;
  else return null;
 }

 /*
 public void add(entity ENT){
  super.add(ENT);
 }

 public void add(String NAME){
  super.ADD(NAME);
 }
 */

}
