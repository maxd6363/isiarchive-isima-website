/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : multiServerCoord.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


import Zdevs.*;
import Zdevs.Zcontainer.*;


public class multiServerCoord extends Coord{

 protected queue  procs;
 protected queue   jobs;
 protected proc   pcur;

 public multiServerCoord(String  name){
  super(name);
  jobs = new queue();
  procs = new queue();
 }

 public multiServerCoord(){
  super("multiServerCoord");
  jobs = new queue();
  procs = new queue();
  //CAUTION: start with port "setup" to test
  addTestPortValue("setup",new entity(""));
  addTestPortValue("in",new entity("val"));
  addTestPortValue("x",new pair(new proc("p",5000),
                       new entity("val")));
  message m = new message();
  m.add(make_content("in", new entity("val1")));
  m.add(make_content("in",new entity("val2")));
  addTest("multiple inputs", m);
  initialize();
 }

 public void initialize(){
  phase = "passive";
  sigma = INFINITY;
  job = null;
  super.initialize();;
 }

 public void showState(){
  super.showState();
  addString("number of jobs: " + jobs.get_length());
  addString("number of procs: " + procs.get_length());
 }

 public void myRepaint(){
  super.myRepaint();
  if( myAtomicDrawPanel != null){
    myAtomicDrawPanel.graph.SetPhaseElapsed(procs.get_length()-5,e/100,phase);
    myAtomicDrawPanel.repaint();
    tf.setText(phase +" e =  " + e +" tL = " + tL + " tN = " + tN );
  }
 }

 protected void add_procs(proc   p){
  procs.add(p);
  pcur = p;
 }

 public void  deltext(int e,message   x){
  Continue(e);
  if(phase_is("passive")){
   for(int i=0; i< x.get_length();i++)
     if(message_on_port(x,"setup",i))
       add_procs(new proc("p",1000));
  }
  if(phase_is("passive")){
    for(int i=0; i< x.get_length();i++)
       if(message_on_port(x,"in",i)){
         job = x.get_val_on_port("in",i);
         if(!procs.empty()){
           entity  en = procs.front();
           pcur = (proc)en;
           //only one job on input can be accepted
           procs.remove();
           hold_in("send_y",100);
           break;
         }
       }
  }
  // (proc,job) pairs returned on port x
  //always accept so that no processor is lost
  for(int i=0; i< x.get_length();i++)
    if(message_on_port(x,"x",i)){
      entity   val = x.get_val_on_port("x",i);
      pair pr = (pair)val;
      procs.add(pr.get_key());
      entity   jb = pr.get_value();
      jobs.add(jb);
    }
  //output completed jobs at earliest opportunity
  if(phase_is("passive") && !jobs.empty())
    hold_in("send_out",100);
  show_state();
 }

 public void  deltint( ){
  if(phase_is("send_out")){
    jobs = new queue();
    passivate();
  }
 //output completed jobs at earliest opportunity
  else if(phase_is("send_y") && !jobs.empty())
    hold_in("send_out",100);
  else passivate();
  //show_state();
 }

 public message    out( ){
  message  m = new message();
  if(phase_is("send_out"))
  for(int i= 0; i< jobs.get_length();i++){
     entity   job = jobs.list_ref(i);
     m.add( make_content("out",job));
  }
  else if(phase_is("send_y"))
   m.add(make_content("y",new pair(pcur,job)));
  return m;
 }

 public void show_state(){
  System.out.println(   "state of  " +  name +  ": " );
  System.out.println("phase, sigma,job,procs : "
                   + phase +  " " +  sigma + " " + job +
    /* job.get_name()  + */ //null.get_name() causes problems
                   " " + procs.get_length());
  System.out.println( );
 }
}


