<?xml version="1.0" encoding="ISO-8859-1" ?>
<feed xmlns="http://www.w3.org/2005/Atom">

	<title>Le site du BDE de l'ISIMA - </title>
	<id>http://www.isima.fr/bde/</id>
	<description></description>
	<updated></updated>



  <entry>
	<title>Enfin ! Vendredi 9 soir�e fin de partiels !</title> 
	<link href="http://www.isima.fr/bde/?102-enfin--vendredi-9-soiree-fin-de-partiels" /> 
	<update>2011-12-06T19:53:15+00:00</update>
	<summary><![CDATA[
		L'�v�nement que tous les ZZ attendent, la meilleure partie des exams, le point culminant de la semaine : la soir�e fin de partiels !!!
Vendredi ce sera la f�te ! Histoire de d�compresser entre ZZ (ou pas!) sur les playlist de Yal et dans une ambiance de folie venez tous au BDE !
	]]></summary> 
  </entry>
  <entry>
	<title>Soir�e clubbin' jeudi 10 novembre</title> 
	<link href="http://www.isima.fr/bde/?101-soiree-clubbin-jeudi-10-novembre" /> 
	<update>2011-11-06T17:47:11+00:00</update>
	<summary><![CDATA[
		Le BDE des ZZ se transforme en discoth�que rien que pour eux ! De la musique qui fait bouger ton corps toute la nuit. Une ambiance comme on aime pour f�ter un weekend de trois jours !
	]]></summary> 
  </entry>
  <entry>
	<title> Soir�e masqu�e - Jeudi 20 octobre</title> 
	<link href="http://www.isima.fr/bde/?100-soiree-masquee-jeudi-20-octobre" /> 
	<update>2011-10-16T22:14:19+00:00</update>
	<summary><![CDATA[
		L'ann�e est d�ja bien entam�e et c'est bient�t les vacances !
Pour f�ter cela, nous vous proposons une soir�e masqu�e au BDE le jeudi 20
octobre (soit le jeudi avant les vacances donc). Vous pouvez donc si vous
le souhaitez venir par�(e) de votre plus beau masque afin de faire la f�te
dans un ambiance un peu plus ... myst�rieuse :)
	]]></summary> 
  </entry>
  <entry>
	<title>Concert inter �coles au BDE de l'ISIMA</title> 
	<link href="http://www.isima.fr/bde/?99-concert-inter-ecoles-au-bde-de-l-isima" /> 
	<update>2011-10-04T12:06:38+00:00</update>
	<summary><![CDATA[
		
	]]></summary> 
  </entry>
  <entry>
	<title>Soir�e fin de partiels ZZ0</title> 
	<link href="http://www.isima.fr/bde/?98-soiree-fin-de-partiels-zz0" /> 
	<update>2011-09-21T02:20:05+00:00</update>
	<summary><![CDATA[
		Vendredi 23 septembre une soir�e avec Polytech au BDE de l'ISIMA pour f�ter la fin des semaines bloqu�es des ZZ0 et des rattrapages des futurs ZZ3.
Venez tous passer une bonne soir�e cette fin de semaine !
	]]></summary> 
  </entry>
  <entry>
	<title>Isimalt</title> 
	<link href="http://www.isima.fr/bde/?97-isimalt" /> 
	<update>2011-07-13T18:52:22+00:00</update>
	<summary><![CDATA[
		Oyez Oyez !

Si vous aimez la bi�re - breuvage datant d'au moins du IVe mill�naire av J-C -, alors ISIMALT est fait pour vous.
	]]></summary> 
  </entry>
  <entry>
	<title>Quizzy</title> 
	<link href="http://www.isima.fr/bde/?96-quizzy" /> 
	<update>2011-07-12T21:42:59+00:00</update>
	<summary><![CDATA[
		Club de l'ISIMA fond� en 2011, compos� de roux, de bretons, de savoyards,
d'�cossais et de gauchers. Mes membres ont soif de connaissance et savent
que J�roboam, Rehoboam, Salmanazar, Balthazar, Melchior, Melchisedech et
Nabuchodonosor sont des souverains.
	]]></summary> 
  </entry>
  <entry>
	<title>La Pourraveth�que</title> 
	<link href="http://www.isima.fr/bde/?95-la-pourravetheque" /> 
	<update>2011-07-12T21:38:18+00:00</update>
	<summary><![CDATA[
		Cin�philes, sinophiles,

si toi aussi tu penses qu'il n'y a rien de meilleur qu'un bon film, si toi non plus tu n'h�sites pas � aller au cin�ma pour mieux profiter d'un bon film, alors ce club N'EST PAS POUR TOI !!
	]]></summary> 
  </entry>
</feed>

