
>From contact@directrecrutement.com  Sat Jan 18 12:56:44 2003

Bonjour, 

Nous vous prions de trouver ci-dessous les offres de stage d�pos�es par les entreprises sur notre site DirectEtudiant.com.
Celles-ci souhaitent proposer sp�cifiquement ces offres aux �tudiants de votre �cole.
Nous vous remercions de leur transmettre ces offres, et restons � votre disposition pour tout renseignement compl�mentaire.
Cordialement,

L'�quipe de Direct Performance
contact@direct-performance.com
01.42.93.17.50 
______________________________________________
Intitul� du poste: Mise au point d'un syst�me de traitement statistique des mesures en plateforme de contr�le des c�bles LAN

R�f�rence : MTR-LAN 

Entreprise : SAGEM SA

Descriptif du poste : Pour notre usine de Montereau, nous recherchons un �l�ve ing�nieur 2�me ou 3�me ann�e.
Tous les c�bles LAN sont contr�l�s en sortie de fabrication. Les donn�es brutes des mesures sont enregistr�es sur fichier texte. L'objectif du stage est de mettre au point un syst�me de traitement statistique de ces donn�es afin d'�laborer automatiquement des indicateurs sur la qualit� des produits en sortie de fabrication.
Vous devez poss�der de bonnes connaissances en statistiques et en programmation (langages utilisables : C, visual basic, HT basic + outils bureautiques microsoft).

P�riode : 4 � 5 mois mini

Fonction/Secteur : Informatique-Math�matique-Telecom

Zone g�ographique : Montereau

Contact : Stephane.Liegent@sagem.com

Lien vers le site 

