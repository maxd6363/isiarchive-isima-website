



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /><link rel="shortcut icon" type="image/x-icon" href="favicon.ico" /><link rel="icon"          type="image/png"    href="favicon.png" /><link rel="stylesheet"        href="./style/isima.css" type="text/css"/><link rel="stylesheet"        href="./style/easyslider.css" type="text/css" media="screen" /><link rel="stylesheet"        href="./style/jquery.lightbox-0.5.css" type="text/css" media="screen" /><link rel="stylesheet"        href="./style/fisheye.css" type="text/css" media="screen" /><script type="text/javascript" src="./js/jquery.js"></script><script type="text/javascript" src="./js/jquery.corner.js"></script><script type="text/javascript" src="./js/easyslider1.7.js"></script><script type="text/javascript" src="./js/jquery.lightbox-0.5.pack.js"></script><script type="text/javascript" src="./js/interface.js"></script><script type="text/javascript" src="./js/fisheye.js"></script><script type="text/javascript" src="./js/isima.js"></script><title>ISIMA - Institut Superieur d'Informatique, de Modélisation et de leurs Applications</title></head><body><div id="ZZheader"><a href="./index.html"><img src="./images/batiments.png" style="width:258px;height:152px;float:left;margin-left:-15px;margin-top:-32px" alt="http://www.isima.fr" /></a><img src="./images/sphere.png" alt="sphere" style="width:120px;float:right;margin-right:30px;margin-top:15px"/><div style="font-size:96px;font-weight:bold;color:white;position:relative;left:50px">I S I M A</div><div style="font-size:150%;color:white;position:relative;left:50px;top:-8px;">&Eacute;cole Publique d'Ingénieurs en Informatique</div></div>   
    <div id="ZZpage">
        <div id="ZZwrapper">
        <div id="ZZcontent">
                       <p>L'<strong>ISIMA</strong> (Institut Supérieur d'Informatique, de Modélisation et de leurs Applications) est une grande école d'Ingénieurs en Informatique en Auvergne.
            Nous recrutons sur le <a href="admission/admission.html?sousmenu=01">Concours Commun Polytechnique</a> et <a href="admission/admission.html?sousmenu=01#dossier">sur dossier</a> é partir de BAC+2 (L2/DUT/BTS).</p>
            <p>Depuis la rentrée 2010, il est possible de faire les deux derniéres années en 
			<strong><a href="enseignement/enseignement.html?sousmenu=01#alternance">alternance en entreprise</a></strong>.<p>
		
            <div id="slider" style="margin:20px auto">
                <ul>
                    <li><a href="enseignement/enseignement.html?sousmenu=03#encours" ><img src="images/banniere/fcregion.jpg"  alt="Recherche candidatures formation continue" /></a></li>				
				    <li><a href="admission/admission.html?sousmenu=01" >              <img src="images/banniere/admission.jpg" alt="Recrutement" /></a></li>
					<li><a href="presentation/presentation.html?page=9" >             <img src="images/banniere/alten2.jpg"    alt="concours alten" /></a></li>            
				</ul>
            </div>
                        
            <div style="clear: both; height: 1px"></div>

            <div id="colonne1" style="width:49%;float:left;">
               <h2>Conférences</h2>
               <p>Entreprises, vous pouvez venir faire présenter un sujet qui vous tient é coeur ou votre entreprise.</p>
               <ul>
			        <li>15 décembre : forum ISIMA avec présentations et stands</li>
			        <li>17 novembre : <a href="http://www.fim-auvergne.fr"i target="_blank">forum FIM</a></li>

					<li><a href="presentation/presentation.html?page=10">Toutes les conférences</a></li>
			   </ul>
               <h2>Actualités</h2>
			   <p>Voici nos <a href="presentation/presentation.html?page=09">actualités</a> : </p>
               <ul>
					<li>1<sup>er</sup> octobre : Remise des diplémes de la promotion 2011</li>
					<li>26 septembre : rentr&eacute;e des troisi&egrave;mes ann&eacute;es</li> 
					<li>12 septembre : rentr&eacute;e des deuxi&egrave;mes ann&eacute;es</li> 
					<li>6 septembre : rentr&eacute;e des premi&egrave;res ann&eacute;es</li> 
					<li><a href="presentation/presentation.html?page=9">Toutes les actualités.</a></li>
				</ul>
		<p>L'accueil des éléves pour la rentrée a lieu en Amphi 2 au Péle Commun, é 9h pour les premiéres années, é 8h30 pour les autres.</p> 
            </div>
            <div id="colonne2" style="width:49%;float:right;">
               <h2>ANELIS</h2>
               <p>L'association des anciens éléves vous propose d'accéder au :</p>
               <ul>
                  <li><a href="http://anelis.isima.fr" target="_blank">Portail des services</a> et au <a href="http://anelis.isima.fr/forum">Forum</a></li>
                  <li><a href="http://blogs.anelis.org" target="_blank">Blog de la Profession ZZ</a></li>
               </ul>
               
			   <h2>ISIMA TV </h2>
			   <p>Retrouvez ISIMA TV sur YouTube :</p>
			   <a href="http://www.youtube.com/user/ISIMATV" style="margin-left:25%"><img src="images/isima_tv_small.jpg" alt="ISIMA TV" title="Chaine ISIMA TV HD" /></a>
            </div> 
           
            <div style="clear: both; height: 1px"></div>

        </div>  
    <div id="ZZsidebar"><p style="text-align:center;padding-bottom:10px"><a href="."><img src="./images/house.png" alt="Page principale | Homepage" style="margin-right:20px" /></a><a href="/isima/index.html?lang=en"><img alt="en" src="./images/en.gif" /></a></p><ul class="navigation"><li class="toggleSubMenu"><span>ISIMA</span><ul class="subMenu"><li><a href="./presentation/presentation.html?sousmenu=01" title="">En bref</a></li><li><a href="./presentation/presentation.html?sousmenu=02" title="">Informatique &amp; Modélisation</a></li><li><a href="./presentation/presentation.html?sousmenu=04" title="">Débouchés</a></li><li><a href="./presentation/presentation.html?sousmenu=11" title="">Forum</a></li></ul></li><li class="toggleSubMenu"><span>Formations</span><ul class="subMenu"><li><a href="./enseignement/enseignement.html?sousmenu=01" title="">Formation d'ingénieur</a></li><li><a href="./recherche/recherche.html?sousmenu=02" title="">Master recherche</a></li><li><a href="./enseignement/enseignement.html?sousmenu=03" title="">Formation continue</a></li><li><a href="./enseignement/enseignement.html?sousmenu=04" title="">DSR</a></li></ul></li><li class="toggleSubMenu"><span>Admission</span><ul class="subMenu"><li><a href="./admission/admission.html?sousmenu=01" title="">sur Concours</a></li><li><a href="./admission/admission.html?sousmenu=01#dossier" title="">sur Titres</a></li></ul></li><li class="toggleSubMenu"><span>Recherche</span><ul class="subMenu"><li><a href="./recherche/recherche.html?sousmenu=01" title="">Une priorité</a></li><li><a href="./recherche/recherche.html?sousmenu=02" title="">Master Recherche</a></li><li><a href="./recherche/recherche.html?sousmenu=04" title="">Laboratoires</a></li></ul></li><li class="toggleSubMenu"><span>Entreprises</span><ul class="subMenu"><li><a href="./entreprises/entreprises.html?sousmenu=01" title="">Relations industrielles</a></li><li><a href="./entreprises/entreprises.html?sousmenu=06" title="">Stages</a></li><li><a href="./entreprises/entreprises.html?sousmenu=05" title="">Formation continue</a></li><li><a href="./entreprises/entreprises.html?sousmenu=04" title="">Taxe d'apprentissage</a></li></ul></li><li class="toggleSubMenu"><span>International</span><ul class="subMenu"><li><a href="./international/international.html?sousmenu=05" title="">Double-Dipléme</a></li><li><a href="./international/international.html?sousmenu=01" title="">Partenaires</a></li><li><a href="./international/international.html?sousmenu=04" title="">Venir étudier</a></li></ul></li><li class="toggleSubMenu"><span>Vie étudiante</span><ul class="subMenu"><li><a href="./vie/vie.html?sousmenu=01" title="">BDE</a></li><li><a href="./vie/vie.html?sousmenu=02" title="">Logements</a></li><li><a href="./vie/vie.html?sousmenu=03" title="">La Région</a></li><li><a href="./vie/vie.html?sousmenu=04" title="">ISIMA TV</a></li></ul></li><li class="toggleSubMenu"><span>Pratique</span><ul class="subMenu"><li><a href="./pratique/pratique.html?sousmenu=01" title="">Contacts</a></li><li><a href="./pratique/pratique.html?sousmenu=03" title="">Plan d'accés</a></li><li><a href="./pratique/pratique.html?sousmenu=02" title="">Annuaire</a></li></ul></li></ul></div>    
        <div style="clear: both; height: 1px"></div>        
        <!-- Partenaires -->
		
		<div style="width:6em;height:4.6em;float:left;padding-left:10px;margin-top:-20px;">
            <a href="http://www.univ-bpclermont.fr/" >
            <img src="./images/ubp.jpg" alt="Université Blaise Pascal - Clermont-Ferrand II" title="Université Blaise Pascal - Clermont-Ferrand II" style="width:100%;height:100%" /></a>
        </div> 
	    <div style="width:89px;height:40px;float:right;padding-top:0.5em;padding-right:10px;">
            <a href="http://www.enaee.eu/" >
            <img src="./images/LogoEUR-ACE.jpg" alt="Label EUR-ACE" title="Label EUR-ACE - habilitation CTI" style="width:100%;height:100%" /></a>
        </div>
		
        <div id="partenaires" class="fisheye">
		<div class="fisheyeContainer">
		    <a href="http://ec.europa.eu/social/main.jsp?catId=325&amp;langId=fr" class="fisheyeItem2" target="_blank">
			   <img src="./images/partenaires/fse.png"  /><span style="color:black">Fonds Social Européen</span></a>
			<a href="http://www.auvergne.org" class="fisheyeItem2" target="_blank">
			   <img src="./images/partenaires/auvergneC.png"  /><span style="color:green">Conseil Régional<br />Auvergne</span></a>
			<a href="http://www.puydedome.com/" class="fisheyeItem2" target="_blank">
			   <img src="./images/partenaires/cg63B.png"  /><span style="color:green">Conseil Général<br />Puy de Déme</span></a>
			<a href="http://clermontcommunaute.net/" class="fisheyeItem2" target="_blank">
			   <img src="./images/partenaires/clerco.png" /><span style="color:blue">Clermont<br />Communauté</span></a>
			<a href="http://www.cnrs.fr/" class="fisheyeItem2" target="_blank">
			   <img src="./images/partenaires/cnrs.png" /><span style="color:darkblue">CNRS</span></a>
			<a href="http://cereales-vallee.org/" class="fisheyeItem2" target="_blank">
			   <img src="./images/partenaires/cereales.png" /><span style="color:green">Céréales Vallée</span></a>
			<a href="http://limos.isima.fr" class="fisheyeItem2" target="_blank">
			   <img src="./images/partenaires/lim.png"  /><span>LIMOS<br />UMR 6158</span></a>
		</div></div>
</div>	
        <div style="clear: both; height:10px"></div>
        </div> <!-- wrapper --> 
     </div>
<div id="ZZfooter">Institut Supérieur d'Informatique, de Modélisation et de leurs Applications<br />Campus des Cézeaux - BP 10125 - 63173 Aubiére CEDEX - France<br /><a href="./sitemap.html">Plan du site</a> - Contactez le <a href="mailto:webmaster [AROBASE] isima.fr">webmestre</a></div><!-- phpmyvisites -->
<a href="http://www.htmlmyvisites.net/" title="phpMyVisites | Open source web analytics" 
onclick="window.open(this.href);return(false);" > 
<script type="text/javascript">
<!--
var a_vars = Array();
var pagename='';

var phpmyvisitesSite = 1;
var phpmyvisitesURL = "http://www.isima.fr/istat/phpmyvisites.html";
//-->
</script>
<script src="http://www.isima.fr/istat/phpmyvisites.js" type="text/javascript"></script>
<object><noscript><p>phpMyVisites | Open source web analytics
<img src="http://www.isima.fr/istat/phpmyvisites.html" alt="Statistics" style="border:0" />
</p></noscript></object>
</a>
<!-- /phpmyvisites --> </body></html>