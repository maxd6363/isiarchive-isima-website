@echo off

if NOT "%1" == "" goto args_count_ok

echo Usage : download.bat [year]
exit /b 1

:args_count_ok
SET /A begining = %1
SET /A ending = %begining% + 1

SET beginingTimestamp=%begining%0101010000
SET endingTimestamp=%ending%0101010000

echo Downloading between %begining% and %ending%...

wayback_machine_downloader isima.fr -d Download%begining% -f %beginingTimestamp% -t %endingTimestamp% -c 4 

