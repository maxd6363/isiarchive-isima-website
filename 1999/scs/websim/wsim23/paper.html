<HTML>
<HEAD>
<TITLE>Simulations in Java</TITLE>
<META NAME="description" CONTENT="SimJava -- A process based discrete
event simulation package for Java, similar to Jade's Sim++, with
animation facilities.">
<META NAME="keywords" CONTENT="simulation Java discrete event animation">
</HEAD>
<BODY BGCOLOR="#ffffff" TEXT="#00000000"
 LINK="#f00000" VLINK="#700000" ALINK="#00ffff">
 
<CENTER>
<IMAGE SRC="simjava.gif" WIDTH=211 HEIGHT=55 ALT="simjava">
<h1> A discrete event simulation library for Java</h1>
<H3>Fred Howell and Ross McNab</H3>
<H4>(extended abstract)</H4>
</CENTER>

<ADDRESS>
<DL>
<DT>Fred.Howell@ed.ac.uk
<dt>phone: +44 131 650 5172, fax: +44 131 667 7209
<DT>Department of Computer Science, The University of Edinburgh,
James Clerk Maxwell Building,
King's Buildings,
Mayfield Road,
Edinburgh EH9 3JZ,
Scotland, UK
<P>
 </ADDRESS>



<H2>Abstract </H2>

<tt>simjava</tt> is a toolkit for building working models of
complex systems. It is based around a discrete
event simulation kernel and includes facilities for
representing simulation objects as animated icons
on screen. Simjava simulations may be incorporated
as ``live diagrams'' into web documents.
<p>
This paper describes the design of <tt>simjava</tt>, how to 
build simulation models using it, and how to animate them
using the <tt>simanim</tt> companion library.

<p>
<h2>The philosophy</h2>

The original motivation for writing simulations in Java
was to allow ``live diagrams'' to be incorporated into
web pages, primarily for technical documentation. 
However experience has shown that Java stands up as a 
viable simulation language in its own right, with 
several advantages over using a C++ based system.
<p>
A <tt>simjava</tt> simulation contains a number
of entities each of which runs in parallel in its
own thread. An entity's behaviour is encoded in Java
using its <tt>body()</tt> method. Entities have access
to a small number of simulation primitives:
<ul>
<li><tt>sim_schedule()</tt>  sends event objects to other entities
via ports.  
<li><tt>sim_hold()</tt> holds for some simulation
time.
<li><tt>sim_wait()</tt> waits for an event object
to arrive.
<li><tt>sim_select()</tt>  selects events from 
the deferred queue.
<li><tt>sim_trace()</tt> writes a timestamped
message to the trace file.
</ul>

<p>
This model is based on the HASE++ simulation library for
C++ [2], which in turn was based on the SIM++ library
from Jade Simulations Inc [4]. It is appropriate for 
any system which can be described as a network of communicating
objects, and has been applied to communications protocols,
parallel software modelling and computer architectures.
 
 
<p>
<h2>How to build a simulation</h2>

The building blocks of a simulation are entities.
Modelling a system involves extending <tt>class sim_entity</tt> 
to include the behaviour of each different type of component.
For example, the following class describes a source entity
which generates 100 events on its output port.

<pre>
     class Source extends Sim_entity {
       private Sim_port out;
       public Source(String name) { 
         super(name);   out = new Sim_port("out");   add_port(out);
       }
       public void body() {
         for (int i=0; i<100; i++) {
           sim_schedule(out,0.0,1);
           sim_hold(10.0);
         }
       }
     }
</pre>

A complete simulation is built by providing a <tt>main()</tt>
method which creates all the entities and joins them together.
Running the simulation produces a trace file of all
the events which occurred, along with any results the
entities have collected about their progress. 
 A set of statistics classes are included in
the package for generating and analysing distributions.


<p>
<h2>How it works</h2>
The basic sequential discrete event simulation algorithm
is as follows. A central object <tt>Sim_system</tt>
maintains a timestamp ordered queue of future events. Initially
all entities are created and their <tt>body()</tt> methods are
run. When an entity calls a simulation function (say 
<tt>sim_hold(10.0)</tt>) the <tt>Sim_system</tt> object
halts that entity's thread and places an event on the future queue
to signify that the hold will complete at (current simulation
time + 10.0). When all entities have halted, <tt>Sim_system</tt>
pops the next event off the queue, advances the simulation 
time accordingly and restarts entities as appropriate. This continues 
until no more events are generated.

<p>
<h2><tt>simanim</tt> - turning simulations into applets</h2>

<tt>simanim</tt> is a companion package to <tt>simjava</tt>
which may be used if a graphical representation and animation
of a simulation model is desired. It is based on 
earlier work on the HASE simulation system [3].
To build a simple animated model the user gives a .gif image
and (x,y) coordinates for each entity. Providing extra .gif images 
allows the entity's icon to change according to its internal state. 
Other entity parameters may be displayed alongside the icon, and updated
as the simulation runs. 
<p>
Two methods of <tt>simanim</tt> create the animation:
<p> <b>anim_init()</b> is overridden to add buttons, 
controls and parameter fields, e.g.:
<p>
<center> <img src="params.gif"></center>

<p> <b>anim_layout()</b> is extended to position 
the entity icons and draw ports, e.g.:
<p>
<center><img src="layout.gif"></center>
<p>

<p>
<tt>simanim</tt> provides a standard panel for controlling
the simulation run:
<p>
<center><img src="panel.gif"></center>
<p>

As the simulation runs, the current simulation time is displayed,
events are shown moving down links and entity icons
switch according to their state.
<tt>simjava</tt> and <tt>simanim</tt> are standard java packages,
and may be used alongside other java packages
for building a simulation application.

<p>Both <tt>simanim</tt> and <tt>simjava</tt>
run concurrently; the output of the <tt>simjava</tt> simulation
code is fed into <tt>simanim</tt>  
to produce the animation.


 
<p>

<p>
<h2>Applications</h2>

The first version of <tt>simjava</tt> was released in September 1996.
Since then there have been several projects making use of it.
At Edinburgh University these include:
<ul>
 <li> Distributed agreement protocols for concurrency control
 <li> Computer architecture interactive learning
 <li> Interconnection network simulations
 <li> Web cache modelling (with the Edinburgh Parallel Computing Centre)
 <li> A VRML version of simanim 
 <li> Producing MIDI from trace files
</ul>
<p>
Other projects include:
<ul>
 <li> Distributed simjava (MITRE corporation)
 <li> Transaction recovery algorithms (Heriot-Watt University, Scotland)
</ul>

<p>
<h2>Conclusion</h2>

<tt>simjava</tt> has shown that it not only is possible to build
discrete event simulations using java, there are many advantages
over using C++ to doing so:-

<ul>
  <li> Simulations can be run anywhere without recompilation.
  <li> Simulations can be included in HTML documents.
  <li> Simpler user interface design.
  <li> Better language.
</ul>

Where simulation speed is paramount, a C++ based simulation package 
currently gives a factor eight improvement in simulation run times [1].
This is entirely dependent on the performance of the underlying 
Java run time system.
<p>
The software package is freely available in full source
code from the home page [5], and it is hoped that it will
prove useful as a basis for java simulation projects. In the 
month before submission of this paper (May/June 1997) the
package was downloaded by 125 companies and universities across 
Europe, Japan, China and the USA.


<h2>References</h2>
<ul>
  <li> [1] <A HREF="http://www.dcs.ed.ac.uk/home/hase/simjava/UKPEWpaper.ps">
R. McNab and F.W. Howell (1996) 
"<b>Using Java for Discrete Event Simulation</b>"
in proc. Twelfth UK Computer and Telecommunications Performance
Engineering Workshop (UKPEW), Univ. of Edinburgh, 219-228 </A>

  <li> [2] F.W.Howell (1996): "<b>Hase++ : a discrete event simulation library for C++</b>." Available from <tt>http://www.dcs.ed.ac.uk/home/hase</tt>

  <li> [3] R.N.Ibbett, P.E.Heywood and F.W.Howell. (1995) "<b>Hase : a flexible toolset for computer architects.</b>" The Computer Journal, 38(10):755-764.
  <li> [4] Jade Simulations International Corp. (1992), "<b>Sim++ user manual.</b>"
  <li> [5] The <a href="http://www.dcs.ed.ac.uk/home/hase/simjava/simjava-1.0"><tt>simjava</tt> home page. </a><tt>http://www.dcs.ed.ac.uk/home/hase/simjava/simjava-1.0</tt>
</ul>