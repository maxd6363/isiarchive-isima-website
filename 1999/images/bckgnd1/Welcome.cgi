#!/usr/local/bin/perl
# Takes a list of files as standard in (piped from ls maybe)
# outputs HTML list of IMG SRC tags for the list...

$bool = 0;

open (Liste,"ls |") || die "Error reading directory\n";

$date = `date`;
chop($date);

printf("Content-type: text/html\n\n\r");
printf("<HTML>\n");
printf("<HEAD>\n");
printf("<TITLE>\n");
printf("ISIMA : Banque d'icones (bckgnd 3/3)\n");
printf("</TITLE>\n");
printf("</HEAD>\n");
printf("<BODY>\n");
printf("<EM>Last Updated : $date</EM>\n");
printf("<CENTER>\n");
printf("<H1>Fonds d'&eacute;cran  Page 3/3</H1>\n");
printf("<FONT SIZE=+1>\n");
printf("Page <A HREF=./images/bckgnd/>1</A>  ||  Page <A HREF=./images/bckgnd/idx2.cgi>2</A><BR>\n");
printf("<B>Pour avoir un aper&ccedil;u d'un fond sur une page enti&egrave;re, cliquer dessus.</B>\n");
printf("</FONT>\n");
printf("<HR SIZE=5>\n");
printf("<TABLE CELLSPACING=20>\n");

while (<Liste>) {
   unless (/Welcome/)  {
        chop($_);
        if (!$bool) { print "<TR ALIGN=CENTER>"; }
        print "<TD><IMG SRC=\"$_\">";
        print "</TD><TD>";
        print "<A HREF=./cgi-bin/test_bckgnd.pl?$_>$_</A>";
        print "</TD>";
        if ($bool) { print "</TR>\n"; }
        $bool = !$bool;
   }
}
printf("</TABLE>\n");
printf("<HR SIZE=5>\n");
printf("<H2>Index construit &agrave; la demande par un script en Perl</H2>\n");
printf("</BODY>\n");
printf("</HTML>\n");
close(Liste);
