/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  helpPanel.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs;
import java.awt.*;
import java.applet.*;

public class helpPanel extends Dialog{
 atomicDrawPanel atomicDrawPanel_ ;
 TextField tf;

 public helpPanel(atomicDrawPanel atomicDrawPanel__){
  super(new Frame(),"Click for Help", false);
  resize(700, 100);
  setLayout(new BorderLayout());
  atomicDrawPanel_ = atomicDrawPanel__;
  show();
  Point p = Graph.AbsLocation(atomicDrawPanel_);
  move(p.x , (p.y+450)%600);
  init();
 }
 public helpPanel(){
  super(new Frame(),"Click for Help", false);
 }

 public void init(){
  Panel b = new Panel();
  b.setLayout(new FlowLayout());
  resize(600, 100);
  b.add(new Button("Toggle Step Mode"));
  b.add(new Button("Restart"));
  b.add(new Button("Step"));
  b.add(new Button("Inject External"));
  b.add(new Button("Set Parameters"));
  add("North",b);
  tf = new TextField(180);
  add("South",tf);
 }

 public boolean action(Event evt, Object obj){
  if(evt.target instanceof Button)
    if("Toggle Step Mode".equals(obj))
	  tf.setText("Press this to alternate between continuous execution and step mode");
  if("Restart".equals(obj))
	tf.setText("Press this to restart execution from the initial state");
  if("Step".equals(obj))
    tf.setText("Press this to trigger the next internal transition");
  if("Inject External".equals(obj))
	tf.setText("A dialog box opens to inject an input");
  if("Set Parameters".equals(obj))
	tf.setText("A dialog box opens to set parameters");
  return false;
 }
}