/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  content.java
 *  Version    :  1.0
 *  Date       :  12-5-96
 */

package Zdevs;
import Zdevs.Zcontainer.*;


public class content extends entity{
 public String p;
 public entity devs;
 public addrclass address;
 public entity val;
 public devs source;

 public content(){
  super();
 }

 public content (String P,entity DEVS, addrclass ADDR, entity VAL,devs Source){
  p  = P;
  devs =  DEVS;
  address = ADDR;
  val =  VAL;
  source = Source;
 }

 public void set_ent(String P,entity DEVS, addrclass ADDR, entity VAL,devs Source){
  p  = P;
  devs =  DEVS;
  address = ADDR;
  val =  VAL;
  source = Source;
 }

 boolean address_is_in(container c){
   return c.is_in(address);
 }

 public void print(){
  System.out.println("content::  port: " + p
                    + " " + ",devs :" + devs.get_name() + " ");
  address.print();
  val.print();
  source.print();
  System.out.println( ")");
 }
}
