/***************************************************************
	ECE 575
	Project:  Distributed Object Computing
	Daryl Hild

File:  MAUxmtr.java

              +---MAUxmtr-----------+
              |      sigma          |
     in       |      phase          |  out
     -------->|      media_state    |----->
              |      buffer         |
              |      back_off_count |
     media_in |    prop_bytes       |
     -------->|    ZERO_TIME        |
              +---------------------+

ATOMIC-MODEL:  MAUxmtr
   assumptions:
      1.  Only input and output ports declared below are valid.
      2.  Only phases declared below are valid.
      3.  Transmission of a frame is modeled as the sending of messages:
          a Preamble, a Frame, and a NoiseBurst on collision detections.
          The Frame contains the IEEE 802.3 pdu fields:  destination, source,
          length and payload.  Receiving a Preamble on the media_in port
          signals carrier detection on the ether.  If the next message is
          another Preamble, or a Frame and a Preamble, a collision is
          detected and collisions are followed by a NoiseBurst; otherwise
          when the next message is a Frame only, its a successful transmission.
      4.  Receiving a NoiseBurst or a Frame only signals the ether is idle.
      5.  New frames require no preprocessing time prior to transmission.
      6.  The ether is initially idle.

   state variables:
      sigma           INFINITY
      phase           passive   (phases: passive, waiting, transmitting, back_off)
      media_state     idle      (media_states: idle, single_carrier, multi_carriers)
      buffer          nil
      back_off_count  1

   parameters:
      prop_bytes      16  bytes //number of bytes propagated in one sim step
      ZERO_TIME       100

   input-ports:
      in
      media_in

   output-ports:
      out

   external transition function:
      for each message on input-port in
         add content-value of message to buffer
      for each message on input-port media_in
         set frame to content-value of message
         case media_state
              idle            if frame==Preamble
                                 media_state = single_carrier
                              else
                                 media_state = idle
              single_carrier  if frame==Preamble
                                 media_state = multi_carriers
              multi_carriers  if frame==NoiseBurst
                                 media_state = idle
      if frame!=Preamble && frame!=NoiseBurst && media_state==single_carrier
           //transmission done; ether idle
           media_state = idle

      case phase
           passive        if buffer is empty
                             Continue
                          else
                             if media_state==idle
                                hold_in waiting ZERO_TIME
                             else
                                hold_in waiting INFINITY
           waiting        if media_state==idle
                             hold_in waiting ZERO_TIME
                          else
                             Continue
           transmitting   if frame!=Preamble && frame!=NoiseBurst
                            && media_state==idle
                             //my transmission done
                             back_off_count = 1
                             remove frame from buffer
                             if buffer.empty
                                passivate
                             else
                                hold_in waiting ZERO_TIME
                          else if media_state==multi_carriers
                             //collision detected; send NoiseBurst
                             hold_in transmitting ZERO_TIME
                          else
                             Continue
           back_off       Continue
           else           error: Unknown phase
                          passivate

   internal transition function:
      case phase
           passive        error: passive phase invalid
           waiting or back_off
                          if media_state==idle
                             hold_in transmitting xmit_time()
                          else  // ether busy; wait for idle
                             hold_in waiting INFINITY
           transmitting   if media_state==single_carrier
                             hold_in transmitting INFINITY
                          else // collision
                             hold_in back_off back_off_time()
           else           error: Unknown phase
                          passivate

   output function:
      case phase
           waiting or back_off
                          if media_state==idle
                             send Preamble on output-port out
           transmitting   if media_state==single_carrier
                             get frame from buffer
                             parse IEEE 802.3 fields from frame
                             send frame on output-port out
                          else //detected collision
                             send NoiseBurst on output-port out
           else           no message

***************************************************************/


import java.lang.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;
import java.util.*;


public class MAUxmtr extends atomic {


	protected queue buffer;
	protected String media_state;
	protected int back_off_count, prop_bytes, ZERO_TIME;


	public MAUxmtr(String MAUname, int PropBytes, int ZeroTime) {
	   super(MAUname+"_xmtr");
	   inports.add("media_in");
	   phases.add("back_off");
	   phases.add("transmitting");
	   phases.add("waiting");
	   prop_bytes = PropBytes;
	   ZERO_TIME = ZeroTime;
	   initialize();
	}


	public MAUxmtr() {
	   super("MAU_xmtr");
	   inports.add("media_in");
	   phases.add("back_off");
	   phases.add("transmitting");
	   phases.add("waiting");
	   prop_bytes = 16;
	   ZERO_TIME = 100;
	   addTestPortValue("in", new pair(
	     new entity("dst1"),
	     new pair(new intEnt(1500),new entity("payload_1"))));
	   addTestPortValue("in", new pair(
	     new entity("dst2"),
	     new pair(new intEnt(500),new entity("payload_2"))));
	   addTestPortValue("media_in", new entity("Preamble"));
	   addTestPortValue("media_in", new entity("NoiseBurst"));
	   addTestPortValue("media_in", new pair(
	     new pair(new entity("prjMau"), new entity("src3")),
	     new pair(new intEnt(1500),new entity("payload_3"))));
	   addTestPortValue("media_in", new pair(
	     new pair(new entity("dst3"), new entity("src4")),
	     new pair(new intEnt(1500),new entity("payload_4"))));
	   initialize();
	}


	public void initialize() {
	   phase = "passive";
	   sigma = INFINITY;
	   media_state = "idle";
	   buffer = new queue();
	   back_off_count = 1;
	   super.initialize();
	}


	public void deltext(int et, message x) {
	    entity frame = new entity("nil");
	    for (int i=0; i< x.get_length();i++) {
	        if (message_on_port(x,"in",i)) {
	            entity pdu = x.get_val_on_port("in",i);
	            buffer.add(pdu);
	            System.out.println(name + " EXTERNAL --> "
	              + "new packet: " + pdu.get_name()
	              + "  packets in buffer: " + buffer.get_length());
	        }
	    }
	    for (int i=0; i< x.get_length();i++) {
	        if (message_on_port(x,"media_in",i)) {
	            frame = x.get_val_on_port("media_in",i);
	            if (media_state=="idle") {
	                if (frame.eq("Preamble"))
	                    media_state="single_carrier";
	                else
	                    media_state="idle";
	            }
	            else if (media_state=="single_carrier") {
	                if (frame.eq("Preamble")) {
	                    media_state="multi_carriers";
	                }
	            }
	            else { //media_state="multi_carriers"
	                if (frame.eq("NoiseBurst")) {
	                    media_state="idle";
	                }
	            }
	        }
	    }

	    if (frame.eq("Preamble")) {
//	        System.out.println(name + " EXTERNAL --> "
//	          + "received: Preamble; media_state: " + media_state);
	    }
	    else if (frame.eq("NoiseBurst")) {
//	        System.out.println(name + " EXTERNAL --> "
//	          + "received: NoiseBurst; media_state: " + media_state);
	    }
	    else if (media_state=="single_carrier" && !frame.eq("nil")) {
	        // transmission done; ether idle
	        media_state = "idle";
	    }

	    if (phase_is("passive")) {
	        if (buffer.empty()) {
	            Continue(e);
	        }
	        else {
	            if (media_state=="idle") {
	                hold_in("waiting", ZERO_TIME);
	            }
	            else {
	                hold_in("waiting", INFINITY);
	            }
	        }
	    }
	    else if (phase_is("waiting")) {
	        if (media_state=="idle") {
	            hold_in("waiting", ZERO_TIME);
	        }
	        else {
	            Continue(e);
	        }
	    }
	    else if (phase_is("transmitting")) {
	        if (!frame.eq("Preamble") && !frame.eq("NoiseBurst")
	          && media_state=="idle") {
	            // my transmission done
	            back_off_count = 1;
	            buffer.remove();
	            if (buffer.empty()) {
	                passivate();
	            }
	            else {
	                hold_in("waiting", ZERO_TIME);
	            }
	            System.out.println(name + " EXTERNAL --> "
	              + "my transmission successful; "
	              + "packets left in buffer: " + buffer.get_length());
	        }
	        else if (media_state=="multi_carriers") {
	            // collision detected; send NoiseBurst
	            hold_in("transmitting", ZERO_TIME);
	            System.out.println(name + " EXTERNAL --> "
	              + "detected collision with my transmission.");
	        }
	        else {
	            Continue(e);
	        }
	    }
	    else if (phase_is("back_off")) {
	        Continue(e);
	    }
	    else {
	        System.out.println(name + " EXTERNAL --> "
	          + "Error: Unknown phase.");
	        passivate();
	    }
//	    show_state(" EXTERNAL --> ");
	}


	public void  deltint() {
	    if (phase_is("passive")) {
	        System.out.println(name + " INTERNAL --> "
	          + "Error: Passive phase is invalid.");
	    }
	    else if (phase_is("waiting") || phase_is("back_off")) {
	        if (media_state=="idle") {
	            hold_in("transmitting", xmit_time());
	        }
	        else { // ether busy, wait for ether to go idle
	            hold_in("waiting", INFINITY);
	        }
	    }
	    else if (phase_is("transmitting")) {
	        if (media_state=="single_carrier") {
	            hold_in("transmitting", INFINITY);
	        }
	        else { // collision
	            hold_in("back_off", back_off_time());
	            System.out.println(name + " INTERNAL --> "
	              + "back_off time: " + sigma);
	        }
	    }
	    else {
	        System.out.println(name + " INTERNAL --> "
	          + "Error: Unknown phase.");
	        passivate();
	    }
//	    show_state(" INTERNAL --> ");
	}


	public message out() {
	    message m = new message();
	    if (phase_is("waiting") || phase_is("back_off")) {
	        if (media_state=="idle") {
	            m.add(make_content("out", new entity("Preamble")));
//	            System.out.println(name + " OUTPUT FUNCTION --> "
//	              + "frame on output-port out is: Preamble");
            }
        }
	    else if (phase_is("transmitting")) {
	        if (media_state=="single_carrier") {
	            entity pduEnt = buffer.front();
	            pair pduPr = (pair) pduEnt;
	            entity dst = pduPr.get_key();
	            entity pay = pduPr.get_value();
	            m.add(make_content("out", new pair(
	              new pair(dst, this),
	              pduPr.get_value())));
//	            System.out.println(name + " OUTPUT FUNCTION --> "
//	              + "frame on output-port out is: "
//	              + dst.get_name() + name + pay.get_name());
            }
            else {
	            m.add(make_content("out", new entity("NoiseBurst")));
//	            System.out.println(name + " OUTPUT FUNCTION --> "
//	              + "frame on output-port out is: NoiseBurst");
            }
	    }
	    else {  // no message
	    }
	    return m;
	}


	public void show_state(String header) {
	   System.out.println(name + header
	     + "state is:  "
	     + "sigma: " + sigma
	     + "; phase: " + phase
	     + "; elapsed: " + e
	     + "; media_state: " + media_state
	     + "; buffer length: " + buffer.get_length());
//	   buffer.print();
	}


    public int back_off_time() {
	    /* Anand Wagle and Vaishali Ghiya
	    */
        back_off_count++;
        Random rand = new Random();
        int temp = rand.nextInt();
        if (temp<0)  temp = temp * (-1);
	    int rem = temp - back_off_count * (int)(temp/back_off_count);
	    int BackOffTime = (int)Math.pow(2.0, (double)rem) * (int)(64/prop_bytes);
	    if (ZERO_TIME!=0)
	        BackOffTime = BackOffTime * ZERO_TIME + ZERO_TIME;
//	    System.out.println(name + "*********DELAY******************");
//	    System.out.println("*********DELAY******************");
//	    System.out.println("backoff time = " + BackOffTime);
	    return BackOffTime;
	}


    public int xmit_time() {
	    int min_frame_len=512,  //IEEE 802.3 field lengths in bytes
	        delimiter=1, dst=6, src=6, len=2, pad=0, checksum=4;
	    // parse frame
	    entity frame = buffer.front();
	    pair   framePr       = (pair) frame;
	    //entity   addressEnt  = framePr.get_key();
	    //pair     addressPr   = (pair) addressEnt;
	    //entity     src       = addressPr.get_key();
	    //entity     dst       = addressPr.get_value();
	    entity   pduEnt      = framePr.get_value();
	    pair     pduPr       = (pair) pduEnt;
	    entity     lenEnt    = pduPr.get_key();
	    intEnt     payld_len = (intEnt) lenEnt;
	    //entity     id        = pduPr.get_value();
	    int frame_len = delimiter+dst+src+len+payld_len.getv()+checksum;
	    pad = min_frame_len - frame_len;
	    if (pad >= 0)
	        frame_len = frame_len + pad;
	    int XmitTime = (frame_len / prop_bytes);
	    if (ZERO_TIME!=0)
	        XmitTime = (frame_len / prop_bytes) * ZERO_TIME;
	    return XmitTime;
	}
}
