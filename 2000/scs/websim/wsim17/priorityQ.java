/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : priorityQ.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


import Zdevs.*;
import Zdevs.Zcontainer.*;

public class priorityQ extends atomic{
 job job;
 protected order q;

 public priorityQ(String  name){
  super(name);
  inports.add("none");
  inports.add("in","job");
  inports.add("inName");
  outports.add("out","job");
  outports.add("outName");
  phases.add("busy");
  q = new order();
 }

 public priorityQ(){
  super("priorityQ");
  inports.add("none");
  inports.add("in","job");
  inports.add("inName");
  outports.add("out","job");
  outports.add("outName");
  phases.add("busy");
  message m = new message();
  addTestPortValue("in",new job1("job5",5000,1));
  addTestPortValue("inName",
  new pair(this,new job1("job",1000,1)));
  m.add(make_content("in",new job1("job1",1000,1)));
  m.add(make_content("in",new job1("job2",2000,2)));
  m.add(make_content("in",new job1("job3",2000,1)));
  addTest("multiple input",m);
  initialize();
 }

 public void initialize(){
  q = new order();
  job = new job("nullJob");
  super.initialize();
 }

 public void showState(){
  super.showState();
  addString("queue length: "  + q.get_length());
 }

 public void myRepaint(){
  super.myRepaint();
  if(myAtomicDrawPanel != null){
    myAtomicDrawPanel.graph.SetPhaseElapsed(q.get_length(),e/100,phase);
    myAtomicDrawPanel.repaint();
  }
 }

 public void  deltext(int e,message   x){
  Continue(e);
  if(phase_is("passive")){
    for(int i=0; i< x.get_length();i++)
      if(message_on_port(x,"in",i)){
        entity ent = x.get_val_on_port("in",i);
        q.add(ent);
      }
    for(int i=0; i< x.get_length();i++)
      if(message_on_port(x,"inName",i)){
        entity ent =x.get_val_on_port("inName",i);
        pair   pr = (pair  )ent;
        entity en = pr.get_key();
        proc pn = (proc  )en;
        if(this.equal(pn)){
          entity ee = pr.get_value();
          q.add(ee);
        }
      }
    entity ent = q.get_max();
    job = (job)ent;
    hold_in("busy",job.processing_time);
  }
  else if (phase_is("busy")){
   for(int i=0; i< x.get_length();i++)
     if(message_on_port(x,"in",i)){
       entity jb = x.get_val_on_port("in",i);
       q.add(jb);
     }
  entity ent = q.get_max();
  job jb = (job)ent;
  if(job != jb){
    q.remove(job);
    job.update(e);
    q.add(job);
    job = jb;
    hold_in("busy",jb.processing_time);
  }
  }
 show_state();
 }

 public void  deltint( ){
  q.remove();
  q.print_all();
  if(!q.empty()){
    job = (job)q.get_max();
    hold_in("busy",job.processing_time);
  }
  else{
   passivate();
  }
  show_state();
 }

 public message  out( ){
  message   m = new message();
  if(phase_is("busy")){
    content   con = make_content("out",job);
    m.add(con);
    con = make_content("outName",new pair(this,job));
    m.add(con);
  }
  return m;
 }

 public void show_state(){
  System.out.println(   "state of  " +  name +  ": " );
  System.out.println(  "name: phase, sigma,q : "
       + job.get_name() + " "+ phase +  " " +  sigma +  " " + q.get_length());
  System.out.println( );
 }
}

