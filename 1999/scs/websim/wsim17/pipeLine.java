/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : pipeLine.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


import Zdevs.*;
import Zdevs.Zcontainer.*;

public class pipeLine extends digraph{

 public pipeLine(){
  super("pipeLine");
  make(4000,4);
 }

 public pipeLine(String name,int proc_time,int size){
  super(name);
  make(proc_time,size);
 }

 private void make(int proc_time,int size){
  inports.add("in");
  outports.add("out");
  pipeCoord co  = new pipeCoord("co");
  add(co);
  Add_coupling(this, "in", co, "in");
  Add_coupling(co,"out",this,"out");

  for(int i = 1; i <= size; i++){
     proc  p = new proc("proc" + i, proc_time/size);
     add(p);
     co.add_procs(p);
  }
  for(entity p = get_components().get_head();p != null;p = p.get_right()){
      entity ent = p.get_ent();
      devs comp = (devs)ent;
      if(!ent.equal(co)){
        Add_coupling(co, "y", comp,"inName"); //use name for routing
        Add_coupling(comp,"outName",co,"x");
      }
  }
  show_coupling();
  initialize();
  //inject("in",new entity("val1"));
  //this shows the need for an interactive
  //input window as in atomic
 }
}