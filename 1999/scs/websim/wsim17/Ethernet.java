/***************************************************************
	ECE 575
	Project:  Distributed Object Computing
	Daryl Hild


File:  Ethernet.java

        +---Ethernet--------+
        |      sigma        |
   in   |      phase        |   out
   ---->|      ether        |----->
        |  prop_bytes       |
        |  ZERO_TIME        |
        |  prop_time        |
        +-------------------+

ATOMIC-MODEL:  Ethernet
   assumptions:
      1.  Only input and output ports declared below are valid.
      2.  Only phases declared below are valid.
      3.  Messages on input port "in" will be of three types: a Preamble,
          a Frame, or a NoiseBurst.
      4.  All nodes are treated as having a worst case IEEE 802.3 distance
          to all other nodes.  That is, 2.5 km apart (five 500 m segments
          with four repeaters).  This also equates to a 25.6 micro sec
          propagation time on a 10 MegaBit ethernet.  Note, this also
          equates to the time for 256 bits, or 32 bytes, to propagate across
          the worst case ether.  Thus, for each message transmitted, the
          model waits the equivalent of 25.6 micro sec (32 byte times) and
          then broadcasts the message back out.
      5.  XX (100) units of simulation time equals time to transmit
          (or propagate) YY bytes on the ethernet medium.

   state variables:
      sigma    INFINITY
      phase    passive     (phases: passive, single_carrier, multi_carriers)
      ether    empty buffer
      clock    0

   parameters:
      prop_bytes  16  bytes     //number of bytes propagated in prop_time (sim step)
      ZERO_TIME   100
      prop_time   if ZERO_TIME==0   prop_time=(32/prop_bytes)
                  else              prop_time=(32/prop_bytes)*ZERO_TIME

   input-ports:
      in

   output-ports:
      out

   external transition function:
      clock = clock+e
      elapsed = e
      for each message on input-port in
         set frame to content-value of message
         case phase
              passive         hold_in single_carrier prop_time
              single_carrier  if frame==Preamble
                                 if ether.empty
                                    hold_in multi_carriers prop_time
                                 else
                                    hold_in multi_carriers sigma-e
                              else  //last bytes from same xmitter
                                 hold_in single_carrier prop_time
              multi_carriers  if ether.empty
                                 hold_in multi_carriers prop_time
                              else
                                 Continue(elapsed)
                                 elapsed = 0
              else            error: Unknown phase
                              passivate
         add (frame,clock) to ether buffer

   internal transition function:
      case phase
           passive         error: Passive phase is invalid
                           passivate
           single_carrier  if ether.front.message==Preamble
                              hold_in single_carrier INFINITY
                           else
                              passivate
                           remove message from ether
           multi_carriers  same = ether.front.clock
                           brst = ether.front.message
                           for each (message,clock) on ether
                              if clock=same
                                 remove message from ether
                           if ether.empty
                              if brst==NoiseBurst
                                 passivate
                              else
                                 hold_in multi_carriers INFINITY
                           else
                              remaining = prop_time - (clock - ether.front.clock)
                              if remaining==0
                                 remaining=ZERO_TIME
                              hold_in multi_carriers remaining
           else            error: Unknown phase
                           passivate

   output function:
      same = ether.front.clock
      for each (message,clock) on ether
          if clock=same
             send message on output-port out

***************************************************************/


import java.lang.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;


public class Ethernet extends atomic {


	protected queue ether;
	protected int prop_time=1, prop_bytes=16, ZERO_TIME=100, clock=0;

	public Ethernet(String  LANname, int setPropBytes, int setZERO_TIME) {
		super(LANname);
		phases.add("single_carrier");
		phases.add("multi_carriers");
		prop_bytes = setPropBytes;
		ZERO_TIME = setZERO_TIME;
		initialize();
	}


	public Ethernet() {
		super("Ethernet");
		phases.add("single_carrier");
		phases.add("multi_carriers");
		addTestPortValue("in", new entity("Preamble"));
 		addTestPortValue("in", new entity("NoiseBurst"));
 		addTestPortValue("in", new pair(
		  new pair(new entity("dst1"), new entity("src2")),
		  new pair(new intEnt(500),new entity("payload_1"))));
		addTestPortValue("in", new pair(
		  new pair(new entity("dst3"), new entity("src4")),
		  new pair(new intEnt(1500),new entity("payload_2"))));
		initialize();
	}


	public void initialize() {
		phase = "passive";
		sigma = INFINITY;
		ether = new queue();
		clock = 0;
		if (ZERO_TIME==0)   prop_time = (32/prop_bytes);
		else                prop_time = (32/prop_bytes)*ZERO_TIME;
		super.initialize();
	}


	public void  deltext(int et, message x) {
	    clock = clock + e;
	    Continue(e);
	    for (int i=0; i< x.get_length();i++) {
	        if (message_on_port(x,"in",i)) {
	            entity frame = x.get_val_on_port("in",i);
	            if (phase_is("passive")) {
	                hold_in("single_carrier", prop_time);
	            }
	            else if (phase_is("single_carrier")) {
	                if (frame.eq("Preamble")) {
	                    if (ether.empty())
	                        hold_in("multi_carriers", prop_time);
	                    else
	                        hold_in("multi_carriers", sigma-e);
	                }
	                else
	                    hold_in("single_carrier", prop_time);
	            }
	            else if (phase_is("multi_carriers")) {
	                if (ether.empty()) {
	                    hold_in("multi_carriers", prop_time);
	                }
	                else {
	                    Continue(0);
	                }
	            }
	            else {
	                System.out.println(name + " EXTERNAL TRANSITION --> "
	                  + "Error: Unknown phase.");
	                passivate();
	            }
	            ether.add(new pair(frame, new intEnt(clock)));
	            System.out.println(name + " EXTERNAL TRANSITION --> "
	              + "received frame: " + frame.get_name()
	              + "   carriers on ether: " + ether.get_length());
	        }
	    }
//	    show_state(" EXTERNAL TRANSITION FUNCTION --> ");
	}


	public void deltint() {
	    if (phase_is("passive")) {
	        System.out.println(name + "INTERNAL TRANSITION --> "
	          + "Error: Passive phase is invalid.");
	        passivate();
	    }
	    else if (phase_is("single_carrier")) {
	        pair queuePr = (pair)ether.front();
	        entity frame = queuePr.get_key();
	        if (frame.eq("Preamble")) {
	            hold_in("single_carrier", INFINITY);
	        }
	        else {
	            passivate();
	        }
	        ether.remove();
	    }
	    else if (phase_is("multi_carriers")) {
	        pair queuePr = (pair)ether.front();
	        entity burst = queuePr.get_key();
	        intEnt same  = (intEnt)queuePr.get_value();
	        int numFrames = ether.get_length();
	        for (int i=0; i<numFrames; i++) {
	            queuePr = (pair)ether.front();
	            intEnt num = (intEnt)queuePr.get_value();
	            if (num.getv()==same.getv())
	                ether.remove();
	        }
	        if (ether.empty()) {
	            if (burst.eq("NoiseBurst"))
	                passivate();
	            else
	                hold_in("multi_carriers", INFINITY);
	        }
	        else {
	            queuePr = (pair)ether.front();
	            intEnt num   = (intEnt)queuePr.get_value();
	            int remaining = prop_time - (clock - num.getv());
	            if (remaining==0)
	                remaining = ZERO_TIME;
	            hold_in("multi_carriers", remaining);
	        }
	    }
	    else {
	        System.out.println(name + " INTERNAL TRANSITION --> "
	          + "Error: Unknown phase.");
	        passivate();
	    }
//	    show_state(" INTERNAL TRANSITION FUNCTION --> ");
	}


	public message out() {
	    message m = new message();
	    pair queuePr = (pair)ether.front();
	    intEnt same  = (intEnt)queuePr.get_value();
	    for (int i=0; i<ether.get_length(); i++) {
	        queuePr = (pair)ether.list_ref(i);
	        intEnt num   = (intEnt)queuePr.get_value();
	        if (num.getv()==same.getv()) {
	            m.add(make_content("out",queuePr.get_key()));
//	            System.out.println(name + " OUTPUT FUNCTION --> "
//	              + "transmitted: " + queuePr.get_key().get_name());
	        }
	    }
	    return m;
	}


	public void show_state(String header) {
	    System.out.println(name + header
	      + " state is:    "
	      + " phase: " + phase
	      + "; sigma: " + sigma
	      + "; clock: " + clock
	      + "; frames on ether: " + ether.get_length()
	      + "; ether: ");
	    ether.print();
	}
}
