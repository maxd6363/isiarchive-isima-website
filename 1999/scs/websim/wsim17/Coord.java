/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : Coord.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */

import Zdevs.*;


public class Coord extends proc{

public Coord(String name){
  super(name,100);
  inports.add("setup");
  inports.add("x");
  outports.add("y");
  phases.add("send_out");
  phases.add("send_y");
}

public Coord(){}

public void initialize(){
  super.initialize();
}

protected void add_procs(devs p){}

}
