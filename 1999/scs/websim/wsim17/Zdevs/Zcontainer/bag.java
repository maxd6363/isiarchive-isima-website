/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  bag.java
 *  Version    :  1.0
 *  Date       :  12-5-96
 *
 *  CLASS BAG:
 *	A container class to perform Bag operations.
 *  bag.java - methods definition of class bag
 */

package Zdevs.Zcontainer;

public class bag extends container{
 /* private */
 static final String classname = "bag";
 /* protected */
 protected boolean is_head(entity ENT){
  if( get_head() == null ){
    if( ENT == null )
      return true;
    else
     return false;
  }
   return (get_head().equal(ENT)); // return true if it is the head
 }                                 // entity in list, else false

 public entity previous(entity ENT){// get to one position before the                                   // entity we are looking for
  if(is_in(ENT))
    for(entity p = get_head();p.get_right()!= null;p = p.get_right())
    if(p.get_right().equal(ENT))
      return p;
  return null;
 }

 public entity previous(String NAME){// get to one entity before entity with name NAME in list
  if(is_in_name(NAME))
    for(entity p = get_head();p.get_right()!= null;p = p.get_right())
       if(p.get_right().get_ent().eq(NAME))
         return p;
  return null;
 }

 public void remove_head(){
  if(get_head() == null)
   return ;
  entity temp = get_head();// saev the location of head
  set_head(temp.get_right());// pointer points to the new head
  //delete temp; // will not delete the object it holds
  decrement_length(1); // decrease the number of elem in list
 }

 public void remove_middle(entity ENT){ // remove one of the middle
  // element in the list
  entity p = previous(ENT);// get to one element before the
                           // to be deleted object
  entity temp = p.get_right();
  p.set_right(temp.get_right());
  //delete temp;
  decrement_length(1);// length of list = length of list - 1
 }

 public void remove_middle(String NAME){ // remove one of the middle
                                        // element in the list
  entity p = previous(NAME);// get to one element before the
                            // to be deleted object
  if(p != null)
    remove_middle(p.get_right().get_ent());
 }

/* public */

 public bag(){
 }

 public String get_classname(){
  return classname; // returns the classification name
 }

 public int number_of(entity ENT){ // how many of this object                                // in container?
  int num = 0;                     // num records number of object(s)
  for(entity p = get_head(); p!= null; p = p.get_right())
     if(p.equal(ENT))
       num++;
  return num;
 }

 public int number_of(String NAME){  // how many of this object
                                    // in container?
  int num = 0;   // num records number of object(s)
  for(entity p = get_head(); p != null; p = p.get_right())
     if(p.eq(NAME))
       num++;
  return num;
 }

 public void remove(entity ENT){ //remove object from container
  if(is_in(ENT))
    if(is_head(ENT))            // if it is the first entity in list,
      remove_head();            // use remove_head() to perform the
    else                        // th deletion
     remove_middle(ENT);        // method to remove entity at middle
 }                              // of the list

 public void remove(String NAME){ //remove entity with name NAME from set
  if(is_in_name(NAME))
     if(get_head().get_ent().eq(NAME))
       remove_head();
     else
       remove_middle(NAME);
 }

 public void remove_all(entity ENT){  // remove all occurance of an
  while(is_in(ENT))                //object from bag
       remove(ENT);
 }

 public void remove_all(String NAME){  // remove all occurance of an
                                      // object with NAME from bag
  while(is_in_name(NAME))
       remove(NAME);
 }

 public bag container_to_bag( container c ){ // copy everything to bag
  bag b = new bag();
  for( entity el = c.get_head(); el!= null; el = el.get_right())
     b.ADD( el.get_ent());
  return b;
 }
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
