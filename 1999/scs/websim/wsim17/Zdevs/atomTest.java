/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  atomTest.java
 *  Version    :  1.0
 *  Date       :  12-5-96
 */

package Zdevs;
import Zdevs.Zcontainer.*;

public class atomTest extends atomic{
 protected int state;

 public atomTest(){
  super("atomTest");
  state = 0;
  phase = "active";
  sigma = 2000;
  addTestPortValue("start",new entity("val"));
  addTestPortValue("stop",new entity("val"));
  initialize();
 }

 public atomTest(String nm){
  super(nm);
  state = 0;
  phase = "active";
  sigma = 2000;
 }

 public void initialize(){
  //super.initialize();
  phase = "active";
  sigma = 2000;
  state = 0;
  super.initialize();
 }

 public void myRepaint(){
  if ( myAtomicDrawPanel != null){
   //myAtomicDrawPanel.graph.setPhaseQuantity(state,e/100);
   //myAtomicDrawPanel.repaint();
   tf.setText(phase + state +" tL = " + tL + " tN = " + tN+" e =  " + e);
  }
  else
    tf.setText(phase + state +
      " tL = " + tL + " tN = " + tN +" e =  " + e );
 }

 public void deltint(){
  state = state + 1;
  phase = "next_active";
  sigma = 1000;
 }

 public void deltext(int t,message x){
     state = 0;
     sigma = 3000;
 }

 public message out(){
   message m = new message();
   content c = make_content("out",new entity("val"));
   m.add(c);
   return m;
 }
}