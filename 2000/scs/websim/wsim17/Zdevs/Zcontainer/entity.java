/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  entity.java
 *  Version    :  1.0
 *  Date       :  12-10-96
 *
 *  CLASS ENTITY:
 *  Every object in the Containers Class Library should be
 *       inherited directly or indirectly from this class.  It
 *    contains the basic query and operation methods for all
 *       other classes.
 *
 * - methods definition of class entity
 */

package Zdevs.Zcontainer;
import java.awt.*;
import java.lang.*;
import java.applet.*;
import java.io.*;

public class entity implements Runnable{

 /* private */
 static final String classname = "entity";  // classification of object
 /* protected: */
 protected Thread myThread;
 //protected Panel myPanel;
 // added by bpz

 public boolean greater_than(entity  ENT){
  return (this.get_name().compareTo( ENT.get_name()) <= 0);
 }

 public void run(){};

 public void start(){
  myThread.start();
 }

 public void stop(){
    myThread.suspend();
 }

 public void resume(){
    myThread.resume();
 }

 public void sleep(int i){
  try{
   myThread.sleep(i);
  }
  catch(Exception e){
   System.out.println("Thread " + Thread.currentThread() + e);
  }
 }


 protected String name;              // name of object
 protected entity right;

 protected String make_name(String NAME){
  //String temp_name = new char [strlen(NAME) + 1];
  String temp_name = NAME + " ";
  return temp_name;
 }

 /* public */

 public entity(){ // for inheritance purpose, no "name" needed
  name = ""; //make_name("");
  right = null;
  myThread = new Thread(this);
  // myThread.start();  //can't do this because
                       //inheritor may not be ready
 }

 public entity( String NAME ){
  name = NAME; //make_name(NAME);
  right = null;
  myThread = new Thread(this);
  //myThread.start();
 }

 /*entity::~entity(){}*/

 public String get_classname(){
  return classname;     // classification of object
 }

 public String get_name(){
  return name;          // name of object
 }

 // added by Vinc 7-30-96

 public void set_name(String nm){
	name = nm;
 }

 public void print(){
  System.out.println( get_name());   // print name of object on screen
 }

 /*public boolean equal(void * ENT){return FALSE;}*/
 // pointers comparison

 public boolean equal(entity ENT){
  return(get_classname().compareTo( ENT.get_classname()) == 0 && get_ent() == ENT );
  // same instance of object?
 }

/*public entity equal_self(void * ENT){return NULL;}*/

 public entity equal_self(entity ENT){
  if(equal(ENT))
    return this;
  return null;
 }

 public entity equal_name(String NAME){
   if( eq(NAME) )
     return this;
   return null;
 }

/*Bool entity::eq(void * ENT){return FALSE;}*/
// name comparison

 public boolean eq(String NAME){ // are they object with the same name?
  return (this.get_name().compareTo(NAME) == 0);
 }  //note this->name doesn't allow recursion

 // another name comparison

 public boolean eq(entity  ENT){ // are they object with the same name?
   return (this.get_name().compareTo( ENT.get_name()) == 0);
 }

 public entity get_ent(){
  return this;   // returns the pointer of this object
 }

 public entity get_right(){
  return right;   // returns the pointer of this object
 }

 public void set_right(entity  e){
  right = e;   // returns the pointer of this object
 }

 public entity copy(){
  entity ent = new entity(get_name());
  return ent;
 }

}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
