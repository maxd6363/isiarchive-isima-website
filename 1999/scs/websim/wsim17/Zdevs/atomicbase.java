/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  atomicbase.java
 *  Version    :  1.0
 *  Date       :  12-5-96
 */

package Zdevs;
import Zdevs.Zcontainer.*;

public class atomicbase extends atomic1 {
//protected int sigma;

 public atomicbase(String name){
  super(name);
  sigma = INFINITY;
 }

 public void initialize(){
  super.initialize();
 // tN = sigma;
 }

 public void Continue(int e){
  if (sigma < INFINITY)
  sigma = sigma - e;
 }

 public void passivate(){
  sigma = INFINITY;
 }

 public void show_state(){
  System.out.println( "sigma : " + sigma );
 }

 public void add_coupling(devs d1, String p1, devs d2, String p2){
  port por1 = new port(p1);
  port por2 = new port(p2);
  ((devs )get_parent()).Add_coupling(d1, p1, d2, p2);
 }

 public void  show_output(){
  if (output == null) return;
   if(!output.empty()){
     //System.out.println( "output of: " + name + ": ");
     output.print_all();
   }
 }
 // DEFINITELY NEED THIS HERE :: inheritance doesn't work

 public void wrap_deltfunc(int t,message x){
  if((x == null || x.empty()) && t != tN)
   return;
  deltfunc(t,x);
  tL = t;
  tN = tL + ta();
 }

 public message get_output(){
  return output;
 }

 public boolean equal_tN(int t){
  return t==tN;
 }

 public void compute_input_output(int t){
  if(t == tN)
    output = out();
  else{
    output = null;
  }
 // System.out.println(name + " output: " );
 // show_output();
 }

 public void print(){
  System.out.println( "model: " + name) ;
 }

 public void hold_in(int s){
  sigma = s;
 }

 public void Cont(int e){
  if(sigma < INFINITY)
    sigma = sigma - e;
 }

 public int ta(){
  return sigma;
 }
}

