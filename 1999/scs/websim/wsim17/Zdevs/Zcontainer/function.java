/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  function.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 *
 *  CLASS FUNCTION:
 *   This class is to perform Function operations.
 *
 *  function.java - methods definition for class function
 */

package Zdevs.Zcontainer;

public class function extends relation{

 /* private */

 static final String classname = "function";

 /* public */

 public function() {
   // for inheritance purpose
 }


 public String get_classname(){
  return classname;   // returns the classname of object
 }

 public void ADD(entity ent, entity value){ // add object to relation
  //if (!key_is_in(ent)){
  pair pr = new pair(ent, value);
  super.ADD(pr);
  //}
 }

 public void add(entity ent, entity value){ // add object to relation
  //if(!key_is_in(ent)) {
  pair pr = new pair(ent, value);
  //super.add(pr);
  ADDRel(pr);
  //}
 }

 // replace "ent"'s "value" with the new "value"
 // it should work as add if there is no key present for function

 public void replace(entity ent, entity value){
  pair  pr = next_key(ent, (pair)get_head());
  if(pr != null)
    pr.set_value(value);  // not quite ok.
  else add(ent, value);
 }

 public void REPLACE(entity  ent, entity value){
  pair pr = next_key(ent, (pair)get_head());
  if(pr != null)
    pr.set_value(value);  // not quite ok.
  else ADD(ent, value);
 }

 public void remove(entity ent){  // remove entity from function
   remove_all(ent);
 }

 // add entity with NAME and "value" into function
 public void add(String NAME, entity value) {
  if(!key_name_is_in(NAME)) {
    entity e = new entity(NAME);
    pair pr = new pair(e, value);
    ADDRel(pr);
  }
 }

 public void ADD(String NAME, entity value){
  if(!key_name_is_in(NAME)){
    entity e = new entity(NAME);
    pair pr = new pair(e, value);
    super.ADD(pr);
  }
 }

// replace entity with name NAME's "value" with the new "value"

 public void replace(String NAME, entity value){
  pair pr = next_key_name(NAME, (pair)get_head());
  if(pr != null){
    super.remove(NAME, pr.get_value());
    add (NAME, value);
  }
 }

 public void REPLACE(String NAME, entity value){
  pair pr = next_key_name(NAME, (pair)get_head());
  if(pr != null){
    super.remove(NAME, pr.get_value());
    ADD (NAME, value);
  }
 }
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
