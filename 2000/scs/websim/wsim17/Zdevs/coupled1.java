/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  coupled1.java
 *  Version    :  1.0
 *  Date       :  12-9-96
 */

package Zdevs;
import java.lang.*;
import java.awt.*;
import java.io.*;
import Zdevs.Zcontainer.*;


public class coupled1 extends devs {
 protected  devsDrawPanel myDevsDrawPanel;
 protected java.awt.List l1;
 protected function ll1;
 protected container buttons;

 public coupled1(String Name){
  super(Name);
  l1 = new java.awt.List(5, false);
  ll1 = new function();
  buttons = new container();
 }

 public void addParamButton(String s){
  buttons.add(new entity(s));
 }

 public container getButtons(){
  return buttons;
 }

 public devsDrawPanel GetdevsDrawPanel(){
  return myDevsDrawPanel;
 }

 public void setDevsDrawPanel(devsDrawPanel ad){
  myDevsDrawPanel = ad;
 }

 public void inject(message m, int e){
  wrap_deltfunc(e,m);
 }

 public function getFn(){
  return ll1;
 }

 public java.awt.List getL(){
  return l1;
 }

 public void addTest(String s,message m){
  message mm = new message();
  atomic firstAtomic = (atomic)get_atomic_components().get_head().get_ent();
  for(entity p = m.get_head();p != null;p = p.get_right()){
     entity ent = p.get_ent(); //since it is element
     content co =  (content)ent;
     mm.add(new content(co.p,this,co.address,co.val,firstAtomic));
  }
  //inject for digraph uses firstAtomic as its source
  //not the most desirable solution
  l1.addItem(s);
  ll1.add(s,mm);
 }

 public void addTestPortValue(String p,entity val){
  String s = p + " " + val.get_name();
  message m = new message();
  m.add(new content(p,this,new addrclass(-1,-1),val,this));
  //inject for digraph uses firstAtomic as its source
  //not the most desirable solution
  addTest(s,m);
 }
}
