/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : atomic1.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;
import java.lang.*;
import java.awt.*;
import java.io.*;
import Zdevs.Zcontainer.*;


public class atomic1 extends devs implements Runnable{
 protected String modelType;
 protected list infoEnts;
 protected int index = 0;



 public list getInfoEnts(){
  return infoEnts;
 }

 protected void addToEnd(infoEnt ie){
  infoEnts.insert(ie,index++);
 }

 public entity withName(String name){
  for(int j = 0;j < infoEnts.get_length();j++){
     entity ent = infoEnts.list_ref(j);
     if(ent.eq(name))
       return ent;
  }
  return null;
 }

  public rand makeRand(int stream){
   if(Prod)
     return new randProd(stream);
   else return new randAnim(stream);
  }


 public int UNIFORM(rand r,int lo,int hi){
  if(Prod)
   return((randProd)r).uniform(lo,hi);
  else return ((randAnim)r).uniform(lo,hi);
 }

 public int EXPON(rand r,int lo){
  if(Prod)
    return ((randProd)r).expon(lo);
  else return ((randAnim)r).expon(lo);
 }

 public int NORMAL(rand r,int lo,int hi){
  if(Prod)
    return((randProd)r).normal(lo,hi);
  else return((randAnim)r).normal(lo,hi);
 }

 protected int locationX = 0,locationY = 0;

 public int getLocationX(){
  return locationX;
 }

 public int getLocationY(){
  return locationY;
 }

 public void setLocationXY(int i,int j){
  locationX = i;
  locationY = j;
 }

 public int getSigma(){
  return sigma;
 }

 public void runInProduction(){
  //show_state();
   while(true){
       boolean event = true;
       if((Input != null && !Input.empty())||min >= tN){
         wrap_deltfunc(min, Input);
         Input = null;
       }
    // System.out.println(get_name() + "tL,tN = " + tL +" " +tN);
   if(event){
   // System.out.println(get_name() + " decrement ");
   myCoord.decrement();
   while(stepMode && pause){
       sleep(10);
       //while w/o sleep grabs too many cycles
   }
   pause = true;
   }
  }
 }

 public void setProd(){
  Prod = true;
 }

 protected boolean Prod = false; //production run

 public atomicDrawPanel GetAtomicDrawPanel(){
        return myAtomicDrawPanel;
 }

 protected int sigma; //why needed here?
  protected  String phase;
    protected    int min = INFINITY;
    protected    int e,E, elapsed;
    protected    coord1 myCoord;
    protected    TextField tf;
    protected    boolean pause = true;
    protected    boolean stepMode = true; //false;
    protected     boolean externalEvent = false;
    protected  atomicDrawPanel myAtomicDrawPanel;
   // protected  atomicPanel myAtomicPanel;
    protected java.awt.List l1,l2,l3;
    protected function ll1;

 public void setAtomicDrawPanel(atomicDrawPanel ad){
  myAtomicDrawPanel = ad;
 }

 /*
 public void setAtomicPanel(atomicPanel ad){
  myAtomicPanel = ad;
 }
 */

 public void kill(){
  myThread.stop();
  //  myAtomicDrawPanel.graph.stop_graphPanel();
  //reinsert when integrating with animation version
 }

 public void setMin(int m){
  //System.out.println(get_name() + " got min ");
  min = m;
 }

 public void setCoord(coord1 c){
  //System.out.println(get_name() + "got coord");
    myCoord = c;
 }

 public void setP(String s){
  //System.out.println("------"+ get_name() + s);
 }

 public void step(){
  pause = false;
  // System.out.println(get_name() + " Step ");
 }

 public void toggleStepMode(){
  stepMode = !stepMode;
 }

 public void inject(message m, int e){
  externalEvent = true;
  E = e;
  Input = m;
 }

 public function getFn(){
  return ll1;
 }

 public java.awt.List getL(){
  return l1;
 }

 public void addTest(String s,message m){
  l1.addItem(s);
  ll1.add(s,m);
 }

 public void addTestPortValue(String p,entity val){
  String s = p + " " + val.get_name();
  message m = new message();
  m.add(make_content(p,val));
  addTest(s,m);
 }

 public java.awt.List getLL(){
  return l3;
 }

 public void newList(){
  // l3.clear();
  l3 = new java.awt.List(5,false);
 }

 public void addString(String s){
  l3.addItem(s);
 }

 public void showState(){
  newList();
  addString("phase: " + phase);
  addString("sigma: " + sigma);
 }

 public atomic1(String Name){
  super(Name);
  tf = new TextField(20);
  // initialize();
  //start();
  l1 = new java.awt.List(5, false);
  ll1 = new function();
  l3 = new java.awt.List(10, false);
  infoEnts = new list();
 }

 synchronized public void initialize(){
  super.initialize();
  elapsed = 0;
  E = 0;
  e = 0;
  System.out.println(get_name() + " Initialized ");
  show_state();
  System.out.println("tL,tN = " + tL +" " +tN);
  myRepaint();
 }

 public void  atomic1Wrap_deltfunc(int t,message x){
  wrap_deltfunc(t, x);
  myRepaint();
 }

 public void Now(){
  if(myAtomicDrawPanel != null)
    myAtomicDrawPanel.Now();
 }

 public TextField get_tf(){
  return tf;
 }


 public void myRepaint(){
  if( myAtomicDrawPanel != null){
    myAtomicDrawPanel.graph.SetPhaseElapsed(5,e/100,phase);
    // myAtomicDrawPanel.repaint();        //remove by Vinc
    myAtomicDrawPanel.redraw();
    showState();
    tf.setText( " tL = " + tL + " tN = " + tN +" e =  " + e);
  }
  else  tf.setText(phase +" tL = " + tL + " tN = " + tN +" e =  " + e );
 }

 public void run(){
  if(myCoord == null){
    modelType="atomic";
    runAlone();
  }
  else if (Prod){
    modelType="digraph";
    runInProduction();
  }
  else{
    modelType="digraph";
    runInDigraph();
  }
 }

 public void runAlone(){
  //show_state();
  while(true){
       boolean event = false;
       sleep(100);
       elapsed = elapsed + 100;
       e = e + 100;
       // show_state();
       myRepaint();
       if(elapsed % 1000 == 0){
         event = true;
         if(myAtomicDrawPanel != null)
           myAtomicDrawPanel.setTfe(e);
       }

     //pause every 1000 in step mode
    // System.out.println("tL,tN = " + tL +" " +tN);

    if(externalEvent && elapsed >= tL+E){
     //System.out.println(get_name() + " INPUT ");
     //Input.print_all();
     e = 0;
     atomic1Wrap_deltfunc(elapsed,Input);
     Input = null;
     event = true;
     externalEvent = false;
    }
    else if (elapsed >= tN){
     e = 0;
     event = true;
     if(myAtomicDrawPanel != null){
        content c = make_content("output",new entity(""));
        myAtomicDrawPanel.graph.SignalOutput(
        myAtomicDrawPanel.graph,this,c);
     }
     atomic1Wrap_deltfunc(tN,new message());
    }
    if(event)
       if(!stepMode){  }
    else{
      while(stepMode && pause){
           sleep(1);
           //while w/o sleep grabs too many cycles
      }
      pause = true;
    }
  }
 }

 public void runInDigraph(){
  //show_state();
  while(true){
       boolean event = false;
       sleep(100);
       elapsed = elapsed + 100;
       e = e + 100;
       // show_state();
       myRepaint();
       if(elapsed >= min ){
         event = true;
         if((Input != null && !Input.empty())||elapsed >= tN){
           e = 0;
           // System.out.println(get_name() + "INPUT ");
           //  System.out.println("min " + min + "tN " + tN +
           // "elapsed " + elapsed);
           // if (Input != null)    Input.print_all();
           atomic1Wrap_deltfunc(elapsed,Input);
           Input = null;
         }
       }

    // System.out.println(get_name() + "tL,tN = " + tL +" " +tN);
    if(event){
      //  System.out.println(get_name() + " decrement ");
      myCoord.decrement();
      while(stepMode && pause){
           sleep(10);
           //while w/o sleep grabs too many cycles
      }
      pause = true;
    }
  }
 }
}
