/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  ensemble.java
 *  Version    :  1.0
 *  Date       :  12-10-96
 */

package Zdevs.Zcontainer;
import java.lang.*;

public class ensemble  extends entity{
 protected container c;
 protected container r;
 protected int count;

 public ensemble(container C){
  c = C;
  r = new container();
  count = C.get_length();
 }

 public void decrement(){
  count = count - 1;
  // System.out.println("count " + count);
 }

 public void stop(){
  myThread.stop();
 }

 public container results(){
  threadsDone();
  return r;
 }

 public void threadsDone(){
  while (count!= 0) { // needed to delay results until ensemble completed
  //System.out.println("count " + count);
  sleep(10);
  }
 }
}

