/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : digrah.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;
import Zdevs.Zcontainer.*;

public class digraph extends coupled{
    public void remove_coupling(devs d1, String p1, devs d2, String p2)
    {
        Coupling.print();
  System.out.println("COUPLINGGGGGGGGGGGGQ");
        port por1 = new port(p1);
        port por2 = new port(p2);
        Coupling.REMOVE(d1,por1,d2,por2);
//this is not removing but causes no null pointer exceptions

  System.out.println("COUPLINGGGGGGGGGGGGQ");
         Coupling.print();
    }
    couprel Coupling;


public digraph()
{
  super("digraph");
  Coupling = new couprel();
 // comp_map = new function();
  inports.add("in");
  outports.add("out");
}

public digraph(String nm)
{
    super(nm);
  Coupling = new couprel();
 // comp_map = new function();
  inports.add("in");
  outports.add("out");
}



public void show_coupling(){ //zeigler new
    Coupling.print_all();
}

public void  add_coupling(devs d1, String p1, devs d2, String p2)
{
        //zeigler new
    Add_coupling(d1,p1,d2,p2);

//internal coupling
  if (!d1.equal(this) && !d2.equal(this)){
    if (!get_components().is_in(d1))
    System.out.println(d1.get_name() + " Warning: entity " + d1.get_name() +
                    " is not a component of " + this.get_name());
   else if (!d1.get_outports().is_in_name(p1))
    System.out.println(d1.get_name() + " Warning: output port " + p1 + " has not been added");

   else if ( !get_components().is_in(d2))
    System.out.println(d2.get_name() + " Warning: entity " + d2.get_name() +
                    " is not a component of " + this.get_name());
   else if (!d2.get_inports().is_in_name(p2))
    System.out.println(d2.get_name() + " Warning: input port " + p2 + " has not been added");

  else if (!d1.get_outports().getClassnm(p1).equals(d2.get_inports().getClassnm(p2)))
    System.out.println("Warning: " + d1.get_name() + " coupling to " +
                  d2.get_name() + " has different value class");

  }
  //external input coupling
  else
  if (d1.equal(this) && !d2.equal(this)){
    if (!get_components().is_in(d2))
    System.out.println(d2.get_name() + " Warning: entity " + d2.get_name() +
                    " is not a component of " + this.get_name());

    else if (!get_inports().is_in_name(p1))
    System.out.println(get_name() + " Warning: input port " + p1 + " has not been added");

    else if (!d2.get_inports().is_in_name(p2))
    System.out.println(d2.get_name() + " Warning: input port " + p2 + " has not been added");

   else if (!get_inports().getClassnm(p1).equals(d2.get_inports().getClassnm(p2)))
   System.out.println("Warning: " + get_name() + " coupling to " +
                 d2.get_name() + " has different value class");

  }
   //external output coupling
  else
  if (!d1.equal(this) && d2.equal(this)){
    if (!get_components().is_in(d1))
    System.out.println(d1.get_name() + " Warning: entity " + d1.get_name() +
                    " is not a component of " + this.get_name());

    else if (!d1.get_outports().is_in_name(p1))
    System.out.println(d1.get_name() + " Warning: output port " + p1 + " has not been added");

    else if (!get_outports().is_in_name(p2))
    System.out.println(get_name() + " Warning: output port " + p2 + " has not been added");

   else if (!get_outports().getClassnm(p2).equals(d1.get_outports().getClassnm(p1)))
   System.out.println("Warning: " + d1.get_name() + " coupling to " +
                 get_name() + " has different value class");


  }
  else System.out.println(get_name() + " Warning: invalid coupling ");
}

public void  Add_coupling(devs d1, String p1, devs d2, String p2)
{
        port por1 = new port(p1);
        port por2 = new port(p2);
        Coupling.ADD(d1,por1,d2,por2);
}


//zeigler new
public void wrap_wrap(int t,message m){
    threadWrap_deltfunc(t,m);
}


public void  tellall_wrap_deltfunc(int tN, message m)
{


 //System.out.println( "DIGRAPH: [" + get_name() +
   //    "] tellall_wrap_deltfunc() tN = " +  tN);

  int nocomps = components.get_length();
  message  comp_input[];
  comp_input = new message[nocomps];
  for (int k = 0; k<nocomps;k++)
  comp_input[k] = new message();


  entity p11;
  for (entity p1 = m.get_head();p1 != null;p1 = p11) {
    p11 = p1.get_right();
    entity ent = p1.get_ent();
    content con = (content) ent;
 //System.out.println( "tell_all_wrap_ after con");
    entity e = comp_map.assoc(con.devs);
 //System.out.println( "tell_all_wrap_ after comp");
    intEnt index = (intEnt)e;
    int ind = index.getv();
 //System.out.println( "ind " + ind);
    comp_input[ind].add(con);

  }

 //System.out.println( "starting calls to wrap_deltfunc");

  int i = 0;
  for (entity p = components.get_head();p != null; p = p.get_right()) {
       entity et = p.get_ent();
       devs d = (devs)et;
       //System.out.println("comp_input[" + i + "] = ");
       // comp_input[i].print_all();
       d.wrap_deltfunc(tN,comp_input[i]);
    i++;
  }

}

public void  threadTellall_wrap_deltfunc(int tN, message m)
//bpz added
{

 //System.out.println( "DIGRAPH: [" + get_name() +
  //     "] tellall_wrap_deltfunc() tN = " +  tN);

  int nocomps = components.get_length();
  message  comp_input[];
  comp_input = new message[nocomps];
  for (int k = 0; k<nocomps;k++)
  comp_input[k] = new message();


  entity p11;
  for (entity p1 = m.get_head();p1 != null;p1 = p11) {
    p11 = p1.get_right();
    entity ent = p1.get_ent();
    content con = (content) ent;
 //System.out.println( "tell_all_wrap_ after con");
    entity e = comp_map.assoc(con.devs);
 //System.out.println( "tell_all_wrap_ after comp");
    intEnt index = (intEnt)e;
    int ind = index.getv();
 //System.out.println( "ind " + ind);
    comp_input[ind].add(con);

  }

 //System.out.println( "starting calls to wrap_deltfunc");

  int i = 0;
  for (entity p = components.get_head();p != null; p = p.get_right()) {
       devs d = (devs)p.get_ent();
       //System.out.println("comp_input[" + i + "] = ");
       // comp_input[i].print_all();
        d.wrap_wrap(tN,comp_input[i]);
         // d.setInput(comp_input[i]);
    i++;
  }
}

public void  threadWrap_deltfunc(int t, message msg)
{


//System.out.println("COUPLED: [" + get_name()+ "] DELTFUNC() tN"+ tN);
  message mess = convert_input(msg);

  if (input == null) {
    input = new message();
  }

  if(!input.empty()) {
        mess.APPEND(input);
  }

  input = null;

  threadTellall_wrap_deltfunc(t,(message)mess);

}

public void  deltfunc(int t, message msg)
{


//System.out.println("COUPLED: [" + get_name()+ "] DELTFUNC() tN"+ tN);
  message mess = convert_input(msg);

  if (input == null) {
    input = new message();
  }

  if(!input.empty()) {
        mess.APPEND(input);
  }

  input = null;

  this.tellall_wrap_deltfunc(t,(message)mess);

}


public void  compute_input_output(int t) {

 //System.out.println( "DIGRAPH: [" +  get_name() +
 //"] compute_input_output()  tN =" + tN);

  tellall_compute_input_output(t);

  // collecting the whole entities to which all imminents have
  // the output

  container mail;

  // gather and convert output
  mail = askall_out();


  input = new message();

  output = new message(); // !!! MEMORY

  which_devs_is_in(mail,input,output);
 //System.out.println("after which_devs_is_in");
/*
    if(input != null && !(input.empty())) {

      System.out.println( "DIGRAPH: [" + get_name() + "] INPUT: "
           + input.get_length());
            input.print_all();

    }
    if (output != null && !(output.empty()))  {

      System.out.println( "DIGRAPH: [" + get_name() + "] OUTPUT: "
           + output.get_length());      ;
          output.print_all();
    }
    */
  }




public void  which_devs_is_in
        (container mail, container resultsin,
           container resultsout)
{
  entity p1;
  for (entity p = mail.get_head();p != null;p = p1) {
    p1 = p.get_right();
    entity ent = p.get_ent(); //since element
    content con = (content) ent;
    //System.out.println("in which");ent.print();
    if (components.is_in(con.devs)) {
      resultsin.ADD(p);
    }
    else {
      resultsout.ADD(p);
    }
  }
}



public container  askall_out()
{
  container results = new container();
  //System.out.println("enter askall_out");
 // container tmpout;
  for (entity p = components.get_head();p != null;p = p.get_right()) {
      entity pp = p.get_ent();
      devs d = (devs)pp;
      message tmpout = d.get_output();
//   if ( tmpout == null)  System.out.println("null");
//    else tmpout.print_all();
  //System.out.println("1 askall_out");

     if (tmpout != null && !tmpout.empty()) {
       entity  p11;
       for (entity p1 = tmpout.get_head();p1 != null ;p1 = p11) {
         p11 = p1.get_right();
         entity ent = (entity)p1.get_ent(); //since really element
         content  co = (content) ent;
  //System.out.println("set");
         set s = Coupling.translate(co.devs,co.p);
      //   s.print_all();
         for (entity pt = s.get_head(); pt != null; pt = pt.get_right()) {
           couppr cp = (couppr)pt;
           entity en = cp.get_devs();
            devs ds = (devs)en;
           port por = cp.get_port();
       //System.out.println("2 askall_out");
       //    devs ds = (devs ) (((couppr )pt).get_devs());
         //  port por = (port ) (((couppr )pt).get_port());
           addrclass tempad = new addrclass(co.address.i,co.address.j);
           entity tempval = co.val;//.copy();
 //bpz
           content  tempco = new content(por.get_name(),ds,tempad,tempval,co.source);
           results.ADD(tempco);
         }


       }

       tmpout=null;
     }
  }
  //System.out.println("results: ");results.print_all();
  return results;
}


public message convert_input(message x)
{
  message  msg = new message();

  if(x.empty())
       return msg;

  for (entity p = x.get_head();p != null;p = p.get_right()) {
      entity ent = p.get_ent(); //since it is element
      content co =  (content)ent;

     set s = Coupling.translate(co.devs,co.p);
     for (entity pt = s.get_head(); pt != null; pt = pt.get_right()) {
        devs ds = (devs ) (((couppr )pt).get_devs());
        port por = (port ) (((couppr )pt).get_port());
        addrclass tempad = new addrclass(co.address.i,co.address.j);
        entity tempval = co.val;	//.copy();
  //bpz
        content tempco = new content(por.get_name(),ds,tempad,tempval,co.source);
        msg.add(tempco);
     }
  }
  return msg;
}

}

