/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : atomicDraw.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;
import java.applet.*;
import java.awt.*;

public class atomicDraw extends Applet {
    atomicDrawPanel myPanel;


public atomicDraw(){
  super();
}
 public synchronized void start(){
        myPanel.start();
    }

    public void init(){
//       		resize(300, 200);
		resize(400, 200);
		setLayout(new GridLayout(1,1));//2,2));//(1, 1));
  myPanel = new atomicDrawPanel(new atomTest("atomTest"));

  add(myPanel);

  show();
    }
}
