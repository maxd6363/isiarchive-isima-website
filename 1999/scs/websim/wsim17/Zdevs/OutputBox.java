/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  OutputBox.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs;
import java.awt.*;
import java.applet.*;

public class OutputBox implements Runnable{
 static int waitTime = 100;
 static int timesToShow = 20;
 private Point end;
 private devs mySource;
 private content myContent;

 public OutputBox(Point start, Point end_,devs d,content c){
  /*
  I create a little dialog box that travels from one point (start) on the screen to another
  (end_).  I create my own thread to avoid interrupting other processes.
  */
  mySource = d;
  myContent = c;
  x = start.x;
  y = start.y;
  end = end_;
 }

 private Thread thread;

 public void start(){
  // Call this to create and start my thread (the corresponding stop() is called by me).
  thread = new Thread(this);
  thread.start();
 }

 private float y;
 private float x;
 private static int ySize = 50;
 private static int xSize = 100;

 public void run(){
  // This is called once start() is called. This actually creates the dialog box and moves
  // it along its trajectory.
  // create the dialog box
  Frame f = new Frame();
  MovingDialog d = new MovingDialog(f,
  myContent.p +":"+myContent.val.get_name(),
  false,mySource,myContent);
  d.reshape((int)x, (int)y, xSize, ySize);
  d.show();
  try{
    thread.sleep(waitTime);
  }
  catch(Exception e){};
  // compute how far the box moves every iteration
  float deltaX = (float)(end.x - x) / (float)timesToShow;
  float deltaY = (float)(end.y - y) / (float)timesToShow;
  // draw the box a set # of times along its trajectory
  for(int k = 0; k < timesToShow; k++){
	 x += deltaX;
	 y += deltaY;
	 d.reshape((int)x, (int)y, xSize, ySize);
     try{
      thread.sleep(waitTime);
     }
     catch(Exception e){};
  }
  d.set_info_location((int)x,(int)y);
  try{
    thread.sleep(waitTime*30);
   }
  catch(Exception e){};
  d.dispose();
  // kill the thread
  thread.stop();
  thread = null;
 }
}

