/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : gpt.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


import java.lang.*;
import java.awt.*;
import java.io.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;

public class gpt extends digraph{

 public gpt(){
  super("gpt");

  atomic m1 = new genr("Generator",1000);
  atomic m2 = new proc("Processor-1",500);
 // atomic m3 = new proc("Processor-2",400);
 // atomic m4 = new proc("Processor-3",800);
  atomic m5 = new transd("Transducer",5000);
  add(m1);
  add(m2);
  //add(m3);
  //add(m4);
  add(m5);

  addTestPortValue("start",new entity("val"));
  initialize();
  show_state();
  Add_coupling(m1,"out",m2,"in");
  // Add_coupling(m1,"out",m3,"in");
  // Add_coupling(m1,"out",m4,"in");
  Add_coupling(m1,"out",m5,"ariv");
  Add_coupling(m2,"out",m5,"solved");
  Add_coupling(m5,"out",m1,"stop");
  Add_coupling(this,"start",m1,"start");
  Add_coupling(m2,"out",this,"out");
  //Add_coupling(m3,"out",this,"out");
  Add_coupling(m5,"out",this,"out");
  show_coupling();
 }
}
