

From: "Serge JUNG" <sjung@corys.fr>
To: <anne.jobert@isima.fr>
Cc: <jgavet@corys.fr>
Subject: Proposition de stage

Bonjour Madame,

Veuillez trouver ci-apr�s une proposition de stage.

Lieu du stage : Corys Tess, Grenoble
Adresse : 72, rue des Martyrs
Code Postal : 38027
Ville : Grenoble
Nom et fonction de la personne qui coordonne les stages : Brigitte CAUSSE
T�l. : 04 76 28 82 29
Fax : 04 76 28 82 11
E-mail : bcausse@corys.fr
Nombre de salari�s : 120
Activit� principale : Conception et la r�alisation d'outils p�dagogiques
pour la formation et les �tudes dans les secteurs de l'�nergie, du transport
et des proc�d�s industriels
Site Web : www.corys.com
Sujet propos� :
Cadre: projet SAMURAIL, logiciel de simulation ferroviaire pour l'�tude et
la planification.
Objectif : mettre en place une nouvelle biblioth�que d'IHM, afin de combler
les limites de l'interface actuelle. L'interface g�re notamment le dessin de
la simulation, la cr�ation/�dition des entit�s repr�sent�es.
Programmation en C++. La biblioth�que d'IHM sera selon toute vraisemblance
Qt.
Dur�e : id�alement 5 mois

Travail � r�aliser par l'�tudiant :
Mise en place de la nouvelle IHM.
L'�tudiant doit �tre assez solide en C++ (de pr�f�rence 3�me ann�e, avec une
exp�rience minimale dans ce domaine).

Nom et fonction de la personne responsable de l'�tudiant pendant le stage :
J�r�me GAVET
T�l. : 04 76 28 82 79
Fax : 04 76 28 82 11
E-mail : jgavet@corys.fr
Montant net de la r�mun�ration ou de l'indemnit� mensuelle : 300 euros (�
noter pour les stagiaires une cantine � tarif tr�s int�ressant)


Serge JUNG, Responsable du Projet SAMURAIL
Anne JOBERT 	Relations Industrielles ISIMA
			les C�zeaux - BP 10125 -
     			63173 Aubi�re cedex 
tel : 04 73 40 50 45
fax: 04 73 40 50 01
mel: Anne.Jobert@isima.fr